This diary file is written by ian tsui e34102113 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-16 #
* 1.In the first class we establish the basic understanding of the class.(such as the course policy)
* 2.We watch the vedio of HOW ABUNDANCE WILL CHANGE THE WORLD - Elon Musk 2017
* 3.We filled the sheet to enroll the class

# 2021-09-23 #
* 1.Firstly, professor talk about conflict and introduce it. Then he make some vivid example about it.
* 2.Itroduce markdown(a kind of font) and git(still trying to understand this website deeply)
* 3.SDGs is the direction of our modern civcilizion

# 2021-09-30 #
* 1.We watched the video learning the fact about news that is misleading. Though i have already understand about it, I still feel quite
    interesting about the video.
* 2.How to protect others intellectual property is something important yet least emphasised. I was glad to learn how to cite imformation.
    It would be quite usefui in my future.
* 3.government should legislate laws to protect people from recieving false imformation, cause it just like a kind of fraud.

# 2021-10-07 #
* For me, this lecture is actually quite interesting. However, it would be easier if the professor can slow down the speed and make 
  the key point about the content that would be great. Because the  lecture today is a little complicated.
* The vakue of the money decreased dramatically over the time. No wonder why the people invest so that they can cope with the inflation.
* The professor's comment about the US central bank " they do terrible" while talk about the decreasing of the purchasing power in US 
  doller is really interesting. I know a little about the macroeconomics, but the history will exam the dicision about QE whether it is 
  beneficial or not.

# 2021-10-14 #
* In summary of the class, i've been inspired by the topic today.
* I began to wounder that some abstract stuffs which I thought important in the past really matter ?
* It comes into my mind that somethings that woundn't affect as should not be taken seriously, such as ideology.
* On the other hand, things such as relationship is essential to me, because without it,there is no differenses between me and animals like monkeies.

# 2021-10-21 #
* In the video of Being "Brilliant Every Single Day", it try to convey the notion that the condition of our mind is also important, though, most of 
  our time often mainly focus on our physical condition.
* The video of "Being Brilliant Every Single Day" is really interesting. The talker use some vivid example to explain its content.
* Exercise is an activity that i do every day, hance I'm really glad to notice the benefits of the exercising in the second video.
* In the third video asigned by the professor, it talk about the systematic error in our healthy care system. As the saying goes "prevenation
  is better than cure." I fully agree of it. Yet, the problem is efficiency of the system been brought by the talker. How to ensure the efficiency
  of medical care after people gotten sick since we punish the company after people got sick.
  
# 2021-10-28 #
* Depression just a state of our mind. When others fall into such state, the only heip we can provide is to listen. We don't have to reason with the
  people in such state, just be patient and be compassionate.
* All the lesson I have learned is that never be shy to ask for help when I'm fall into the loop of depression.
* I was shock when hearing the claim that you don't have to be doctor Phill. You don't have to be responsible to others mind state. Hence, we just have to treat others as usual.
  be our true self.
  
# 2021-11-04 #
* Actually I totally agree of that it's time to make change. The backlash of global warming gradually come to us. 
* In my opinion, enforrcing the whole society to make the change that fits to the situation is imposible. Cuase the global warming is the cost of
  developing our civiclization. Just like smoking or abusing alcohole, people know it is wrong, but it can't be stoped. The only thing we can do is to slow it down.
* After watcing the video provided by the class, 
* I know less about myself than I thought? This is interesting. I have already know that long time ago. I discover from media.
  Media can limit our view and choices which lead to the change of our mind, which also can be view as manipulated.
  
# 2021-11-14 #
* The class today is mainly focus on supergroup presentation. It's my pleasure to be the speaker.
* I think there still has a lot of room for my presentation to improve. Especially in terms of the fluency of speaking English.
* Others perform great, I have learned from others presentation.
* It's shame that the time is not enough for teacher to teach the scheduled content of the the class today.

# 2021-11-25 #
* We have watched a lot of video recorded by the students. 
* We learned how to communicate and reach a consensus from the process of interacing with mine group members while doing our class work.
* The process of reaching consensus was quite interesting.
* The second video played by the teacher was shocking. The traits of Millennials described in the video was commen for the teenagers today. It was really a problem that we should avoid. I don't want to make the same mistakes.