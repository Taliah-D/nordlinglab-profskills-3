This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Winny Chang H24064064 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* I learned Wright's Law in the first class.（products will become cheaper from year to year）But I wonder if it is based on labor exploitation and environmental pollution. Do we still want it? 
* Fast fashion is an example. Do we need a lot? Do we need to chase the better one forever? 

* Glasl's model of conflict escalation. It let me know communication is important. Only communication can let us have a win-win situation.
* The professor gave other groups a lot of feedback. And I also made some errors they made in my ppt.
* I think I can learn a lot from this course.

# 2021-09-30 #

* I gave a presentation, and got some feedback from the professor. For example, it is not enough to cite the website, you also need to write the author's name and the last time you visited the website.
* My original teammates all dropped the course, and thanks for the TA, now I have two new teammates.
* There are three videos we need to watch this week, and my favorite one is "The best stats you've ever seen" by Hans Rosling. My major is statistics, and after studying for some years, I was bored with statistics, but after watching the video, it rebooted my passion for statistics. I hope I can be a statistician like Hank.
* The videos that professor always gave us always changed my mind a lot. I appreciate the professor's effort to the course and feel lucky to take the course.

# 2021-10-07 #
* I don't know many problems of the quiz that the teacher asked us to answer about the financial system.
* There is income inequality in Taiwan.
* It’s lucky to find my teammate easily this time.
* Although all my teammates are Taiwanese, we still  communicate in English. And it is a good way to practice English.
* I learned that the Gini index is an indicator to measure income inequality. The higher, the more inequality. 

# 2021-10-14 #

* 	financial system homework
	* 	workers need to ask for an adequate salary which should grow at the rate of inflation.
	* 	purchasing power should use the national dollar, and don't use the US dollar, because it will affect the inflation of the US dollar.
	* 	BigMac is good at comparing countries, but we just want to see the individual country.
	* 	what gives money its value: should be found by article, journal or textbook.
	* 	consumer debt/corporate debt should find its true values rather than its percent to GDP.
	* 	The country has a high balance sheet doesn't represent the country is rich. In stead, if it is too much, the country may have some financial problems. Because it means the central bank should buy many assets to make the price stable
* video
	* 	Every day, find somebody that you think is undeserving of your compassion and give it to them, because they're the ones who need it the most.
	* break the mirror that makes you see yourself better than you really are.
* English usage
	* 	compare apples and oranges means 把毫不相干的人或物硬湊在一起作比較.
* new instruction from teacher
	* 	record 2~4 minutes video to presentation
# 2021-10-21 #

* It is funny to hear that there is a myth in Malaysia that says Taiwanese English is proficient. 
* Exercise 3 to 4 times a week, 30 minutes.

# 2021-10-28 #

* I totally agree with “Being strong is killing us.”  I don’t need to be perfect, just be better than yesterday.
* Having feelings isn’t a sign of weakness. Feeling means we’re human.
* If my burden gets too heavy, I can ask for a hand. I can talk to the professors, our school therapists.

# 2021-11-04 #
* Video do you really know why you do what you do?
	* meaning is more to life than being happy.
	* meaning: 
		* belonging
		* purpose: not what you receive, but what you give
		* transcendecnce
		* storytelling: You can edit your life story, use another way to interpret your life story.
* For the course project, I  have learned how to go to NCKU library to find the master paper.


# 2021-11-18 # 
* There is no class for this week.
* I forgot writing diary last week.
* Through the half of this semester, I think my English speaking skills is better than before.

