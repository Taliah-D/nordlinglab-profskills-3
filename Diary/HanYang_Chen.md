This diary file is written by Hugh Chen E14086282 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23
* I am still learning how to use this GIT tool. Seems to be easy, but quite difficult actually.

# 2021-09-30
* I've been injecting the vaccine for the covid-19, making me so uncomfortable to pay attention in class.
* It's a pity to barely learn anything because of the situation, but I really can't control.

# 2021-10-07
* Introducing about financial system, deposits and loans.
* Gold is considered to be the ultimate safe haven.
* When a bank's deposit-to-loan ratio is greater than 1, the bank has loaned out every cent of its deposit.
* "Fungible"
* The purchasing power of each currency has dropped exponentially for the last hundred year.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.

# 2021-10-14
* Fascismo-法西斯主義
* "Extremist"
* Human are the most imaginative.

# 2021-10-21
* How you think affects how you do. How you feel affects how you think. Your emotions affect how you feel. Your physiology affects your emotions.
* Seems to be easy to control people if learn enough about psychology or the brain.
* Exercising improves your brain.
* Exercise 3~4 times per week at least, with 30 minutes every time.
* True health care system could save us from unnecessary costs and risky procedures.

# 2021-10-28
* We were assigned into a super group in class. I can strongly feel the differeance between a small group of three people and a big one with more than ten. The progress of discussing is much slower although we can hear more different opinions from others.
* From a presentation from another group, I learned the importance of staying away from cellphone before going to bed and sleeping enough. The former leads to a better sleep and the latter benefits my body in many ways.
* From the TED talks, I learned that to listen is a good way to interact with a depressed person. I agree with that because a friend of mine, who suffers with depression, told me that he felt great when talking to me. Although I actually didn't say anything but just listened during the chat.

# 2021-11-04
* When trying to make yourself happy, the process might actually make you more anxious. I can kind of feel that when I feel happy playing games all day, but also feel anxious for wasting so much time on playing.
* I took the second BNT vaccine this weekend at Cheng Kung University Hospital. It feels really different compared with the first time at a small clinic. The manpower is sufficient and the moving line is clear. This time, I only spent less than 20 minutes on the whole process, while last time at the clinic, I used more than 30 minutes just waiting in line. This experience makes me see the difference when working in different resourses.

# 2021-11-11
* 躺平主義(lying flat)- not to keep fighting, but to relax and simply stay alive(?
* This week when I got into a bus, there was no other seats, so I sat on the priority seat. But when I tried to yield the seat to those who actually needed, they rejected. It made me think if I should sit there in the first place.


