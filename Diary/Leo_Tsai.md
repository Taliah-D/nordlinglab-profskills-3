This diary file is written by Leo Tsai E14083137 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* This course operates in a whole different mannar than moodle, so it'll take some time for me to keep up the pace.
* I really learnt a lot from the exponential growth topic last week, hoping I can develop valuable skills along the course.
* I think exponential growth in green energy will lead to a world of abundance and everlasting.
* Glasl conflict escalation model subdivides human interaction into multiple stages, which is a novel idea to me.
* Scrolling on google to find out whiat GIT is, and found out Linus stated that git can be interpreted as anything, depends on your mood.


# 2021-09-30 #

* I find this course not as stressful as other courses, and we can also cultivate invaluable skills rather than academic knowledge in the mean time.
* A good statistic can be visualiaed with vivid diagram or gragh rather than cold numb data.
* In statistics, scale is everything. Once you change the scale(axes), you can change the story.
* I learned how to cite articles correctly today, which is definitely useful in all directions.
* Newsvoice topic: How to be free from mental conditioning caused by society
* Humans copy what other people are doing and it happens through repetitive content.
* The average person thinks that they get married at a certain age.
* Take a nine-to-five job instead of starting one's own buissness.
* The subconscious manufactures your behavior through analyzing the repetitive content in your environment.
* The solution is to hack your subconcious mind and exchange bad habits with healthy one.
* It's mainly a repetition fight.
* Cited from https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/


# 2021-11-04 #

* Professor talked about financial system today, and introduce some interesting facts about deposits and loans.
* Gold is often one of the assets that is considered to be the ultimate safe haven, which value usually doesn't fluctuate dramatically.
* If one bank's deposit-to-loan ratio is greater than 1, which implies that the bank has loaned out every cent of its deposit.
* It is the danger zone because it has no reserves to pay customers for demand deposits.
* It's interesting to learn that whenever Chinese's new year has come, the amount of money government held increased drastically
* due to the Chinese's envelope custom.
* I also learned a tricky word "fungible", which is something may be replaced by another equal part or quantity.
* Money is the typical instance that is fungible.
* The purchasing power of each currency has droped exponentially for the last hundred year.
* The inflation of currency has become so severe that I can't help wonder what can we afford to buy
* with a thousand dollars in the next or two decads to come, perhapes not even a drink.
* If the government keep letting the inflation sabotages our currency purchasing power, then we must publish a new currency.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.
* Every politician tells that we must need to fix this gap, and makes promises to their voters.
* However, who really commited to their promises after elected, who ever really has taken this issue seriously.
* Tragedies will still happen, and the gap will grow faster and faster until the poor can't withstand the extremely uneven 
* distrubution of resources.

# 2021-10-14 #
* I gave a presentaion today, and recieve a lot of advice from professor.
* I used to think that giving presentation in English is quite intimidating, however now I think that only be overly prepared can make my nerve relax. 
* Facism is used as a kind of general purpose abuse or they confuse facism with nationalism.
* Facism tells me that my nation is supreme and that I have exclusive obligations towards it.
* Now data is replacing both land and machines as the most important asset.
* Facism is what happens when people try to ignore the complications and to make life too easy for themselves.

# 2021-10-21 #

* Today's vedios are very interesting, and energetic. Explaning how people can be brilliant and toward a better version of ourselves in scientific way.
* Often people think that changing behavior will change the result, howevert it's not the case.
* There's 4 levels beneath behavior, which are thinking, feelings, emotions, and physiology from top to buttom.
* The most enchanting part is Dr.Alan explains that emotion and feelings are completely different things.
* "Emotion" is energy in motion, and those energy is come from physiology, including all the signals transmitted by our senses.
* In conclusion, emotion is all the signals inclusive of electric signal, pressure wave, and son on.
* feeling is the awareness of those energy flow in our body.
* In order to be brillient everyday, we must start to control those levels beneath behavior.
* Exercise is truly the key to a healthy life, working out consistently makes our brain release adequate amount of endorphins
* Endorphins can not only reduce the perception of pain, it also triggers a positive feeling in your body, such as boosting mood.
* I build a habit of exercising regularly in the past 3 years, it was hard to embrace the soreness accumulated in muscle at first.
* however, as endorphins began to release, I started to enjoy the process over pain.
* After all the hard work I put in, I realized that my body begin to shape, and become more fit than ever, which bring me sense of achievement.
* Now, I'm more confident, and proud of being recognized as a disciplined person, moreover, an atheletic person.

# 2021-10-28

* Today I volunteered to present in the super group, which was a brave move to me. I recalled that professor said
* "Only by pushing yourself out of comfort zone, which is presenting publicly can you overcome the anxiety of public speaking."
* Therefore, I must not hesitate to say yes when there's oppertunity to present. As awkard as it may be in the begining, I'll get good at it eventually.
* People often get terrified when thinking about talking to people with depression.
* The reason is that people think that they might put their foot in it, and accidently make the situation worse.
* However, it is not the case. We can be a listener to depressed people, perhaps bonding with them will make us feel great.
* Besides, we might learn something from them. Depressed people are people too, they are not monster, their brain just don't function normally like us.
* As a successful black woman, Nikki Weber suffered from depression and what's worse was that she thought having depression is a shame.
* Until her beloved nephew commited suicid beacuse of depression, she finally couldn't stand the pain of not speaking out the feeling of depression.
* She began to resort to doctors, talk to family and friends, eventually she overcome the depression she was ashamed of.
* Nikki even started to share her own experiences and taught us how to fight mental illness.
* Mental health is as important as physical health, and they complement each other.
* I think I have sort of experienced depression when I was in high scool beacuse of the incompetence of handling pressure.
* However, I learned some valuable lessons from this hard time, which is "Everything will be fine in the end."
* Nothing is there to stop you from doing the things that you want except your own fear.
* Fears are fabricated and are conjured up, and are lies. Most of our fears won't even happen, we're just too stupid to scar ourselves!


# 2021-11-04 #
 
 * Having a meaningful life can make people more dedicated and more resiliant, and they do better at work or school and even live longer.
 * Belonging, purpose, transcendence and story telling are the four pillars for a meaningful life.
 * Happiness comes and goes, we can't rely on the persuit of happiness as our goal of life, there's going to be something that upset us.
 * However, having meaning in life make us have something to hold on to, we can rely on it no matter we're mptivated or depressed.
 * Meaning fills the emptyness in our heart, make us more resilent to adversities.
 * As time flies, I've become a junior in college, I'll soon have to enter the workforce, but all I have now is knowledge in books.
 * I literally have no idea about how to apply the academic knowledge to work, I can only calculate math, solve physics problems and draw free body diagram.
 * I have little to noun social skills such as communication, critical thinking, and so on.
 * Toxic work environment will do one no good, if one is working in a toxic environment just to get hgh salary, then it's the stupiest idea.
 * You can't spend your money if you don't have good health, well, except on your medical bill.
 * My solution to a fullfilling job is as followed: 
 * Work on myself hard to enrich my kowledge and skills, therefore I'll grab the oppertunity once it comes.
 * Stay consistent on the promise I make, and be relentless to persue my goal.
 * Never settle and relax, need to keep grinding, and hustle beacuse that's the difference between greatness and mediocrity.


# 2021-11-11 #

* Noise pollution is common nowadays, no matter it's heavy traffic or construction sites, exposing oneself for too long is harmful to our body.
* Exposing ourselves to noise pollution continuously will reduce our life span and either develop mantal illness or physical diseases.
* Using steel reinforced cement is creating lots of CO2 emissions as well as producing, transporting, repairing and daily consumption.
* Energy consumption of steel reinforced cement building is larger than others since people need to turn on air conditioner to cool down the temperature inside.
* It's cool to learn that green magic building awarded world's greeenest building, and this building is in NCKU!
* I also learnt that using natural materials such as woods for building will not reduce CO2 recycling.
* Making woods as our building prevent us to generate more CO2 emissions further.
* The course project topic for our group is methods to empoer people, we found it hard to understand at first, however we managed to overcome the obstacles.


# 2021-11-25 #
* Today is physical class, initially I felt reluctant to attend the physical cousre since online course is more convenient and comfortable.
* The new classroom is astounishing, and I was amazed by the number of monitors, theree are two in front, two in back and four aside.
* All monitors are synchronized to the professor's labtop.
* Mechanical department seems to have earn a fortune lately.I'm thrilled to see our depaetment has a state-of-the-art equipment installed in the classroom.
* Today's vedio watch topic "How we need to remake internet" by Jaron Lanier is fantastic, Lanier is the pioneer of the virtual reality and a computer scientist as well.
* Lanier said that we, humankind are responsible for the distorted values nowadays.
* Another vedio watch "Millenials in the workplace" by Simon Sinek really illustrated how and why children in this generation has gotten so unsympathetic and indifferent.
* Biggest problem among this generation is the lack of authenticity since the absence of real interactions with people, deep connection is seldom formed.
* The world seems to have connected everthing through the internet, however, people relationship with each others has become less bounded than ever.
* When ever we encounter any adversity, we choose to scroll the internet instead of talking to real person for real advice.
* Besides the internet problem, the way how parents adore thier children is the main reason why suicide rate is climbing.
* Parents nowadays have over protected thier children, being a helicopter parents who always clean up choldren's mess for them.
* This ends up letting children incable of dealing stresses on their own, once they face any difficulties, they choose to embrace instant gratification as an escape.
* Today we had in person discussion during the course, which is the reason why I've changed my mind that I now do believe physical class is way more better than the online one.
* I'm more involved and engaged in the group discussion, and the efficiency is 10 times faster than the online course.
* This leads to the point where Simon said real bounding is formed through real interactions and communcations, I couldn't aggree to it anymore as I attended today's physical course.
* To sum up, this course is definitely in the top of the list of my favorite classes that I've taken in this semester
 

