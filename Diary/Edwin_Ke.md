This diary file is written by Edwin Ke in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Tried to come up with a few mediators, but turns out I was confused with the definition of a mediator.
* Our group didn't get to do the presentation. Next time, we will be faster to grab the chance.
* The layouts of a PowerPoint presentation can really affect its outcome.
* This is my first time using BitBucket. Might take a while to get used to this tool.

# 2021-09-30 #

* Think it would be a bit difficult for every person to present 3 times, since many students have enrolled in this class. 
* Guess I'll just have to be fast and grasp the chance to present.
* There is always going to be negativity on social media, so it's important to learn to not be greatly affected.
* Since I am a highly sensitive person, I definitely need to be aware of the negative effects of social media.
* Gave a presentation and received lots of feedback from the professor. Glad to have learned more about how to make better slides.
* The world is getting better gradually, but even with the problem of famine being solved, I believe we still have a long way to go.
* Learned that one good way to deal with fake news is by comparing alternative sources.
* Statistics can be inaccurate amd misleading if the surveys conducted to produce them are ambiguous.

# 2021-10-07 #

* Gave a presentation regarding why statistics are important today. 
* Was delighted to see that the professor liked my conclusion: "Statistics are important, because they help us love people."
* Learned that the Congress in the United States has been demanding reports on UFOs from government agencies.
* Just because a piece of news about the existence of aliens has been proven fake, we can't rule out the possibility of their existence entirely.
* Did a survey on financial knowledge. Astonished by the fact that there were a bunch of things I didn't know, even though I use money every single day.
* The reason why there is always a surge in the amount of money taken out from banks during winter in Taiwan is because of the traditional custom. 
* There are five desirable properties of money. However, according to the professor, the New Taiwan Dollar is not durable.
* Was surpised to find out that when people make loans, new money is actually created simultaneously.
* The purchasing power of the New Taiwan Dollar has been decreasing. No wonder it is not durable.
* Ｑuantitative Easing is an unconventional policy that may spur economic activity in a short time. However, it has high risks.
* There are eight tasks this week. Better get started.

# 2021-10-14 #
* Felt incredibly frustrated during class today because my Internet connection somehow kept crashing.
* The TED talk videos were fantastic, although I would have enjoyed them a lot more if my WiFi was stronger.
* Have heard of the word "fascism" before but have never understood the meaning of it till this class.
* Genuinely moved by the second TED talk video, in which a man named Christian Picciolini shared his story of getting out of a hatred movement and helping others to do so.
* Agree with a fact mentioned by Christian-When faced with unfamiliar stuff, we tend to fear or even hate it.
* I remember disliking a British singer named Jess Glynne before even listening to her songs, because I had never heard of her before. But after hearing her music, she ended up becoming one of my favorite artists.
* I guess my experience above proves the aforementioned fact.
* Sometimes when we look into the backgrounds of seemingly ill-minded people, we would build up compassion for the hurt they went through.
* If potholes are left neglected or overlooked, they might gradually affect our mentality over time, no matter how hard we try not to think about them.
* People's repulsive behavior often reflects the struggles they have in themselves. For instance, Extremists often feel a lack of belongingness.
* The foundation of one's identity should be established on self-confidence and self-love, instead of hatred towards other people.
* Hopefully we will all learn to have sympathy and love for each other, even with scars over our hearts.

# 2021-10-21 #
* Watched a TED talk video today during the second part of the class. It's very thought-provoking.
* It makes sense that we can't think well under stress, since there would be a physiological reaction called the cortical inhibition.
* Deep breaths help calm us down. I personally think it is indeed beneficial. I've tried it a couple times before exams, and I did feel a bit less stressed afterwards.
* Watcher another few videos during the last past of the class. The first one makes me want to start exercising regularly right away.
* Turns out that exercise has a great number of benefits for one's health. For instance, it helps protects one's brain from diseases.
* To me, the biggest gain from exercising is not weight loss, but a boost of energy that motivates me to do more.
* As a person who easily gets anxious under tremendous pressure, I need to develop a habit of exercising on a daily basis.
* The second video is interesting. It encourages the idea of updating our health care system by preventing the occurence of diseases.
* Although it may contribute to privacy concerns, I think it would be a good idea for health care systems to manage our personal health data.
* By securing the health of each person, we would be able to cut down financial burdens of the system.
* It was no surprise when half of the class raise their hands when asked if they have felt anxious or stressed over the past week.
* I personally have experienced minor climate anxiety before when I saw many leaders of large countries neglecting the importance of protecting the environment.
* In my opinion, if we are able to do our best in protecting the Earth ourselves, then it will help relieve climate anxiety.
* Since we have a limited impact on what other people do(ex: the government), we should focus on doing our part of the responsibility of keeping the Earth healthy.
* I look forward to learning how to handle depression better next time!

# 2021-10-28 #
* Did my first Supergroup project today. Took a lot of teamwork but I'm glad we made a good presentation.
* I've actually looked forward to today's lecture for so long, because I am personally very interested in handling depression better.
* I've learned from the first TED talk that those who suffer from depression don't lose their desire to connect with people, just their ability.
* In my opinion, telling someone who suffers from depression to get over stuff is pointless, because everybody understands that. It's just that in certain circumstances, people don't feel like doing so.
* Sometimes when helping depressed friends, we tend to take negative responses too personally. However, we shouldn't really anticipate positive reactions, because it's not always possible for a depressed person to change immediately.
* One of the ways to help a depressed friends is by allowing him/her to accompany us for special occasions.
* I once asked my friend out for a walk in the night after he went through a terrible time. He ended up sharing with me some past and private experiences that bothered him continuously. My company gave him the comfort he needed at that period.
* I think one of the reasons why we face troubles when helping a depressed friend is because we tend to put ourselves in a higher position than our depressed friends, which often leads to feelings of imbalance within their minds.
* Therefore, it we could make our depressed friends feel belonged and valuable, they would be more likely to open up and feel better.
* I like how Kevin Briggs talks about his stories in a calm and soothing tone. I'm glad that he was able to save so many lives.
* When we tell a person we know how they feel, it's usually not true. We can never ever completely understand how another person feels.
* I can really relate when Kevin mentioned that he saved a man from jumping off a bridge just by listening to him. 
* Most times when we feel depressed, we don't really want advice. What we simply need is someone who's willing to listen to our feelings.
* Instead of seeing the Golden Gate Bridge as a graveyard of many people who committed suicide there, Kevin believes that it is a connection between people that brings hope, and I think that's beautiful.
* Depression is often viewed by many as a display of weakness, and that's why many depressed people aren't willing to seek professional medical help.
* "Being strong is killing us." This is a simple sentence, but it makes me rethink of the hard times I went through. 
* When I was in a dark place, I tried hard to put a smile on my face because I didn't want others to worry about me.
* Nowadays, whenever I feel like it, I would go out with a friend, talk about my struggles, and even cry in front of him. I decided I wasn't going to hide the vulnerable side of me anymore.
* I've heard somewhere that depression can't be fully cured. Not really sure if it's true, but I choose to believe that we can still live out our lives, even with depression.
* If we don't face depression, we will often numb ourselves with self-medication to avoid the pain or the void inside us, and that often leads to very bad results.
* As I grow older, I've learned to express how I feel to people I trust and love, and it has benefited me a lot.
* About a couple months ago, I went through the end of a relationship plus a heavy battle with vocal inflammation. Because of this, I became very depressed, and my bad mental state led to even more physical discomfort. I'm glad that I'm in a better place now.
* I received help from a couple friends and took as much time I needed to understand what exactly happened to me. Therefore, I was able to gradually improve my health and life.
* In the process of handling depression, I realized that there is so much more beauty in life, and I don't need to let one adversity or even a person ruin it for me.
* I look forward to using what I've learned from my past expeeriences to help and love those struggling with depression.

# 2021-11-4 #
* Shared my story about depression today. I had a very terrible experience with dealing with vocal cord inflammation, and after taking care of my mental health, I was able to gradually improve.
* I actually had to deal with the aftermath of a heartbreak during that time. Was extremely difficult for me.
* I'm glad the professor gathered our comments and results on the anxiety survey, because they help us to understand we are not alone.
* Family will always be your backbone, no matter how bad or tiring life is.
* It's plausible that cooperativity has become one of the main factors that companies consider when hiring new people. 
* I really enjoyed the TED talk by Emily Esfahani, because I can relate to what she's trying to appeal to the audience.
* Belonging comes from being intrinsically valued for who you are, not from who you hate or what you believe. To me, belonging is a form of unconditional love.
* Emily mentioned that storytelling plays a key role in our lives, because since we are the authors, we have the freedom to decide how we interpret everything in our lives.
* I used to view all the traumatizing experiences and adversities as curses that were meant to punish me, but in recent years, my mindset has changed.
* Looking back at my life, I believe that all the troubles and pain of my past were blessings, for they made me grow and evolve as a better person with more strength and love.
* Even though I'm still in the process of recovering, I can feel myself growing more mature, and that is something I am proud of.
* The TED talk by Petter Johansson was interesting. It turns out most of the time we don't really know why we commit certain actions or make certain decisions.
* I believe it is important that we learn to truly understand ourselves, even though it takes time and honesty.

# 2021-11-11 #
* Our group project helped me to realize the relationship between traffic noise and health problems.
* I used to think that the outcome to long-time exposure to traffic noise would only be hearing loss, but turns out that it can also lead to cardiovascular diseases, strokes, and even dementia.
* To be honest, NCKU has a pretty serious traffic noise pollution problem. Many faculty buildings are located near large roads.
* Group 9 came up with the idea of nonviolence for methods to empower people. I personally support this idea. It's a lot better than creating riots.
* Martin Luther King Junior is a legendary figure who I look up to, because he managed to diminish the public's hatred toward black people through peaceful protests.
* The recent parades that celebrate the LGBTQ community are a form of nonviolence too. 
* It's pretty mind-blowing that cow dung has many amazing benefits in various aspects, including health and environment.
* I didn't know that eating indigenous foods helps to prevent greenhouse gas emissions, since transporting food requires petroleum-based fuels, and many fertilizers are also fossil fuel-based.
* I think what we could have done better in our project is that we shouldn't merely focus on evidence that supports our topic. We should also keep an eye out on false information or questionable experiment results.
* Construction noise was a major issue when I was living in the school dormitory. The sports field next to us was constantly being used as a construction site.
* Hopefully, the school would try implemening some strategies to reduce the affect of traffic noise.

# 2021-11-25 #
* Today's the first time we have a physical class. Pretty cool. The classroom is way too advanced.
* I think Simon Sinek's talk really provoked a lot of thoughts in my mind.
* There's nothing wrong with social media, but an umbalanced use of it leads to a worldwide crisis.
* It is indeed true that we live in a world where we value instant gratification. 
* I can understand that we like the feeling of getting what we want instantaneously, but the tendency of having this desire actually kills us. 
* Personally, I can relate to what Simon said regarding the mentality of millenials. When we see a message pop up, we immediately feel a bit better due to our desire of being noticed or cared about fulfilled.
* In the future, when I go out to have dinner with my partner, I'll try leaving my cellphone at home. I believe this is beneficial to stabalizing a relationship.
* I think that before meetings, we tend to focus on our cellphones to avoid awkwardness, but this actually weakens the bonds between people.
* Having actual, meaningful conversations helps build trust and the base of a great friendship.
* Spending too much time on social media causes a person to become more insecure and susceptible to changes.
* The first video was a bit boring. Maybe speeding up the video would help.
* There was an in-class task that required us to come up with laws. It was a bit difficult.
* To be honest, it would be quite a challenge for us to stay connected and free at the same time, but at least we can try proceeding toward this goal.
