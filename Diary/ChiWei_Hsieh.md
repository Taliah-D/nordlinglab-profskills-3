This diary file is written by Chi Wei, Hsieh in the course Professional skills for engineering the third industrial revolution.
Chinese name: 謝濟緯
Student ID: I54091149

# 2021-09-23 #

* Still trying to figure out what git is about
* Since electric cars are gonna be a lot cheaper, I'm gonna start investing in Tesla right now

# 2021-09-30 #

* The ability to identify fake news is important
* Never blindly believe nor reject any given information
* The method used to require statistical data has a great impact on the validity and credibility of the results 
* Sources are important as they provide authenticity to data

# 2021-10-07 #

* Loans create money, that's just so cool, I wanna make money too
* Income inequality is terrible, the rich should help the poor more

# 2021-10-14 #

* People who lack the emotional support that they need might go on dark paths
* Ignorance leads to most conflicts
* We should know ourselves better, so that we don't get fooled by extremist propagandas

# 2021-10-21 #

* Doing exercise stimulates the brain to have better function
* Being aware of our physical status allows us to have better performance
* Me as an athlete since a young age can confirm that the above two statements are true in some ways

# 2021-10-28 #

* Many people didn't contribute to the supergroup project, sad
* This week's course is about depression. My ex had depression, I didn't know the right way to deal with that back then. (Plus we had a long time seperated and broke up)
* I like the thought that the way to help depressed people is not by bridging people together, but closing the gap
* Listening and caring is what most depressed and suicidal people need
* Many depressed people see being depressed as being week, hiding their problems from others
* Be yourself, whether you're depressed or not. There is no need to feel bad for who you are

# 2021-11-04 #

* Besides with family, we spend most of our times at school or at work
* Having a good work/school environment is crucial for our well being
* At home we have parents and siblings, at work we have employers and coworkers, at school we have teachers and classmates
* As humans, it is nature for us to want a sense of belonging
* With guidance from mentors and company from friends, it is easier for us to blend into a group
* When encounter problems, we speak with our parents. So when we meet questions at work or school, it is totally fine for us to communicate with our employers or teachers
* Sometimes when we want to change the environment, one way is to change from ourselves

# 2021-11-11 #

* Today's a special holiday!
* I've been extremely busy in the past two weeks, and I'm glad that many affairs are coming to an end by this weekend
* Noise is a serious problem, you don't need to live in an urban neighborhood to experience it, it's everywhere
* A video from one of the group presentations said that noise is subjective, that reminds me of the people singing songs poorly at the dormitory bathroom every night. Their singing is horrible! Yet they're singing so loud!
* I've watched something on TV a few months ago that said something similar to what the professor said about locking CO2 in wooden buildings, it's a cool concept. The TV program also said that some countries are building skyscrapers with wood!

# 2021-11-25 #

* Many people say that we find more and more cases of depression anually because people are more open to the subject. I, however, believe the major cause of it can attributed to the growing problems of society
* There is no denying that social media connections actually creates difficulties in human interactions
* Every day we look into our phones, searching recognition on social media platforms, but ultimately, they are not real connections
* I feel this void deeply everytime I use my phone too much while not speaking to actual people
* For people who were born in this generation of rapid technology grownth, everything comes and go so fast, we start to take many things for granted. People cherish what they have less, to the point they don't know what to cherish
* The best fruits take time to grow. Being honored for hard work and persistence is the best reward ever. Spend more time with people, form bonds, the results must be fruitful