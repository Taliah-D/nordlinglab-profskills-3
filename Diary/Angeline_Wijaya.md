

This diary file is written by Angeline Wijaya E64095346 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* I get startled when I got into this course because it's my first time to get taught by a foreign professor, so cool to experience it
* I got to know what is bitbucket about and how to use it although I'm still a little confused, but I bellieve I will get used to it later
* In the videos that professor asked us to watch, one of the video title is “ How to seek the truth in the era of fake news” , It taught me that truth and wisdom cannot come just by the degree 

# 2021-10-07 #

* Quote of the day “trust yourself before trust other”
* I just meet my groupmates last week and now I feel sad to change groupmates again.
* I know more functions of money that I didn't have notice and think before. Beside for medium of exchange, there are unit of account and store of value which have diffrent meaning from each other.
* I find something funny for today course about the teacher question " how to print millions of ntd legally?" and the question is by taking a loan hahaha.

# 2021-10-14 #

* I don't know much about finance, but after the presentations of several groups, I got to undestand more aout it.
* Things that caught my attention is the video of Ted's talks about the descent into America's-neo Nazi movement, the sentence "Hatred is born of ignorance, fear is its father and isolation is it's mother. I don't know why but it related so much with my daily life problems.
* I have something curious about the statement that said facism is different with the nationalism, because from what I disgest, facism is one of the way for a person or people that believe in it to show their nationalism.

# 2021-10-21 #

* Actually ,it feels so good when there's a teacher who cares about our mental condition.
* I know more about what the neurosciene study is.
* whenever we were doing exercises, it was actually changing our brain without us knowing it. 
* Our physiology determines feeling; emotion is interpretation of feeling; emotion has to do with cognitive processes, thinking. And last our thinking influenced most of our daily performances.

# 2021-10-28 #

* We did a supergroup discussion today but our group was so silence, as a result we were late for our presentation today
* I've been looking forward to today course because I would like to see the old of me from the perspective of new of me
* From my opinion I agree that those who suffer from depression actually didn't lose their desire to connect with people, but they lose the ability. 
* In my past society, I used to not to care anything about my friendship because it's fine through the year.But when I arrived here, society hits diffrent. You can't really trust people easily.
* I can relate that if there's your friend who feel depressed or stress, all they need is someone who is willing to listen all of their worriness.
* Most of the people that were thinking suicide,all they know are they wouldn't hurt another person. They just want their own pain to end. But for me it's not really true to think like that. You didn't know that in this life there's still someone who adore and love you so much, that you wouldn't notice  you will acidentally hurt them if you commit suicide.
* Being strong like somebody who have no feeling is killing us, and having feelings isn't a sign of weakness. Feelings mean we're human. When we happy we smile and laugh and we were sad we cry. We're human, we are free to expressed our own feelng.
* Actually life is beautiful if we enjoy it. Although sometimes it's messy, and unpredictable. But "everything will be okay" when you have somebody who support you.
* If anything is going hard, ask for help. We're all human and we know that humans as social beings mean we cannot live alone. If humans do not relate or interact with other human beings, then that person cannot be said to be human. 
* It's great lecture to understand.

# 2021-11-04 #

* I'm really grateful could listen both of our classmate story about depression, it's such a pleasure and great experience for me who can listen to the awesome and special people like them.
* I'm still a little confused about the purpose of the manga x diary and how to use it actually.
* For me, living in a good environment is immportant, but if you can survive in a bad environment , then you are awesome.
* From my opinion,  the most important thing to live a healthier life is the mindset of ourselves, we should have a great mindset like how we think for every problem we faced in our daily life. It's really up to us to think the problem we faced in a positive way or negative way. If we want to think it in a positive way, then I believe we can get more positive response better than we think it in a negative way.

# 2021-11-11 #

* All of the presentaions of today supergroup disussed about many problems that happened in our soicety in this era.
* In the presentation of the 1st group I realized that I have a sleep inconsistency where I don't have enough of sleep time during weekdays and I paid it by oversleeping during the weekend that actually resulting the lack of concentration during learning.
* From my point of view , I know that noise pollution really disturb our concentration when doing something, but in here I just know that noise pollution is a serious problem where it is also unconsciously may affect our mental health condition.
* I really love the story about the Martin Luther King where he led a movement of non-violent, peaceful protests to fight racial injustice in the United States in the 1955 year. I love this inspirational story because in the really early year like that he could think so deep and pay attention to this problem where not all of the people nowdays may pay attention to it and still being racist.