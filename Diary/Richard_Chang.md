## 2019-09-19

* For the conflict behaviour part.
* Teacher said that Dampen approach is the approach that those who has harmony view mostly use is kinda true because I'm that type of person. I geuss Collaboration approach is the best(maybe not the most efficient) way for a discussion.
* The way teather present at the conflict escalation model is funny.
* Actually I had known a little about GIT.But I was not sure how it really works,and this course just teach it! I thought this system is only used on coding,but now it seems like it can be used on several field,even like writing diary.
* About the future, actually, it seems to me that this course is somehow too optimistic so far. But I am really looking forward to that future,I expect that I may change my mind in the future!
* I can't figure out why the Wright's law is not as famous as Moore's law. It seems like more accurate and general. Maybe is controlled by the media? So that it can be probitable by some person?

## 2019-09-26

* Today's topic was fake news. It is really improtant to have a ability of filtering what we see or hear.
* The TED talk had a fresh idea about objectivity which I had kept thinking for a long time.  It said that objectivity is not make everything equal but treat everything equally. We can have a perspective and spread that as long as we're willing to listen to various voices.
* I couldn't help but keep thinking about today's politic. Nowadays, every media is truly filled with fake news. Not only about that, there're also non-objective news that biased strongly. And the audiences just choose what they want to heard,  condemn the opposite perpective media as well.

## 2019-10-03

* Economy is a subject I've nearly never study. This course was kinda fresh to me.
* The second video was talking about dept, credit, and cycle of economy.
* It seems likt we are being through the recession stage recently.
* One of the chart in the slide compared the income growth between 1980 and 2014 is horrifying. Many people says that the gap between rich and poor keeps growing is true.

## 2019-10-10
* You know what?
* A day off!!

## 2019-10-17

* Today's topic is talking about extremism.
* Videos today are really rich and are from different perspectives.
* The first video indicated that nationalist and extremism are different. The extremism take nation or race prior to anything else. It's from the overall viewpoint to talk about extremsim.
* The second video was more emotional, which talking about extremism with more personal perspective. And it really touched my heart that I didn't expected.
* About the third video, I'm not sure how to give my opinion. I couldn't understand the topic without subtitle or caption because of my poor Englisg listening ability. That's kinda embarrassing.

## 2019-10-24

* Today's lecture was pretty different from previous lecture.
* Presentations were given by "supergroups" that consist of three group. It's kinda fresh, making a slide on the class was troublesome though.
* And we had an experiment this time. It's fresh and funny too.
* Today's video was talking about exercising can make a person work better and boost his/her memory and mood.
* I'm stand with the speaker's claim. Recently, I exercise regularly because of an exam. I'm not sure if I concentrate better, but I don't need that much sleep to keep waking up instead of more sleep time.

## 2019-10-31
* Today's topic is talking about depression.
* How to help those in depression is really a hard question because it seems that it doesn't have the best solution.
* About the bridge which the most people committed suicide, I wish I have a chance to go there in the future.
* Some speakers also had mental problem, and they decided not to hide but try to spread out their situation.
* Letting people know that depression is not a symbol of "weak" may be one of the most important things we can do.
* Because most of time, those people in depress fear to confess his/her condition.

## 2019-11-07
* Lecture today is talking about happiness (or fulfullness).
* The first video is about an exoerience. He let people choose the prefer one between two profile pictures. And then, give them the chosen one and ask why they prefer that one. But sometimes he will change the picture, and give them the another one. And most of time, the subjects didn't notice it. Then, they would make up the reasons after the dicision.
* The second video is talking about "forgiveness". This was a more emotional one. After a tragedy, maybe choosing forgiveness is a better way instead of hatred.
* The third video is about life meaning. And it said there are four pillars of a meaningful life:belonging, purpose, transcendent, and storytelling.
* The last two pillars are fresh idea to me. Especially the last one:storytelling. I think it really can change a person's life. But it may not be as easy as it sound. Because if we just try to fool ourselves, I don't think it will change the life. We need to try to really believe in the story.

## 2019-11-14
* Today's topic was about legal system.
* Legal system is important and essential to be known, tbh, it was more boring than usual though.
* At first, we defined the "legalese".
* It means that we need to have a specific meaning for some terms. And the meaning is rigorous, with no grey area, in case of misunderstanding.
* We had some example in legalese, e.g. corporation, income tax, etc.
* The video said that if a cop want to investigate something with us, we can defend our own right.
* Most of time, we guess police are always right and obey their commands with no doubt. But actually it is not always true.
* the priority is filming at the cop for saving evidence.
* "Innocent until proven guilty" is also an inportant concept.

## 2019-11-21
* Today it said about the Internet.
* The first video said the Internet is developing in a wrong way.
* He said we expect the Internet is free and accessible for anyone, at the same time, we expect some tech entrepreneurs that can dent into universe. But this two things are contradiction.
* Nowadays, those advertisements on the free platform maybe a manupulating tool, rather than "advertisement".
* The video said there's a way may solve it, that is, streaming media.
* The second video was about effects of smartphone or social media. The speaker was humorous.
* He stated the bad effects of the social media. And so much people addict to it.
* Social media is not so infamous that we shouldn't touch it. The matter is balance.
* We should drop down the smartphone and talk to each other more often, and that's how connection works.

## 2019-11-28
* Today we talked the rules we had made during this week.
* We know that it's really hard to make a rule that everyone agree wuth it.
* And it's also difficult to make a feasible rule.
* We discussed about the "rule" in different scenario, like rules for the the world with only one person, or rules for people in a finite region.
* The video was about news and civilization.
* It said governments always want to interfere the market when falling in inflation, and it never made things better, but worse.
* People would altomatically adjust their behavior as prices floating.
* Prices is information.

## 2019-12-05
* I didn't go to the class because I had a seminar that day.
* And the topic was about planetary boundaries.
* There are plenty of planetary boundaries.
* These are the limit that we should try not to break.
* I think the most famous planetary boundary is the climate change.

## 2019-12-12
* Today's topic was about the third industrial revolution, but we did not go in the topic.
* We had a debate like a penal.
* Each team must represent their own boundary, so it took almost one and half hour to go.
* And then we had question time, debate(argument) time and voting time.
* We had a pretty different way to have a class.
* I think this lecture was really funny, I geuss it's maybe the best lecture in this semester.
* I kinda hope we can have more lecture like this.

## 2019-12-19
* Today's way how lecture go was just like last time, it was really interesting. Actually I hope most of levture were taught like this.
* We presented about what is the third industrial revolution.
* And then professer asked us to divided into three group, as cipitalist, worker, and enviromentalist.
* We had a debate about a topic among what we had presented.
* We chose "inequality".
* And we debated for amount of time.
* After the debate, we had some video about productivity and success.

# Daily Diary
## 2019-12-19

## 2019-12-20
* I didn't feel no productive today.
* I wake up at noon, and then had a class. And do nothing. 


## 2019-12-21
* I felt productive today.
* I went to library at ten o'clock and left at nine o'clock.

## 2019-12-22
* I felt really productive today.
* I went to new study builing from the morning and kept studying until ten half at night.

## 2019-12-23

## 2019-12-24
* Not much productive.
* I felt unproductive because I wake up at noon.
* I may try to wake up earlier.

## 2019-12-26
* We spent whole class to present.

## 2020-01-02
* Today we discussed three solutions about last week's topic.
* And then we chose several kind of feasible solution.
* We voted some solution as next week's topic.