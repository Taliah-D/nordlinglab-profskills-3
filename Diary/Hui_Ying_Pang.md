This diary file is written by Pang Hui Ying F94085038 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30
* We should learn about how to verify good data and bad data but don't just believe and receive what we see.
* Fake news are all around us, verify the facts of news is a very important things.
* Same as satistics, we should learn to do research about any imformation we got by ourselves.

# 2021-10-07
* I would like to learn more about finance because my father is a business man, but I dont even know about something about finance.
* Under this covid-19 pandemic, many business had faced many problems about finance, it would be better for us to learn about situation nowadays.
* Learning about finance and stock price is a very important things, especially for foreign students who need to exchange money form our country.

# 2021-10-14
* Fictional stories maybe a great stories that we yearn for, due to its unrealisties.
* Everyone try to yearn for those fictional stories that seem impracticable, but we learn lessons during these process of yearning.
* Malaysia is a country that full of promises which offer by famous politician, I don't think those promise will be achieved if our rotten goverment still manage the policy.
* Believe between a good goverment and people can improve the growth of country, so that everyone can live in a better world.

# 2021-10-21
* Controling emotion is great hard task, we should learn to control it.
* To ensure we get great performance, we should learn to control our emotion, so far we can get ourselves in better feeling and have positive thinking.
* There is a claim about exercise can make us more focus and attention, but most of the people just feel tired and sleeping after exercise.
* Believe between a good goverment and people can improve the growth of country, so that everyone can live in a better world.

# 2021-10-28
* Depression such a serious mental disease because it can easily lead to suicide.
* Suicide can help people, who suffer from mental illness, escape the darkness hell, and this is not a great solution but just a avoidance.
* Everyone must have own darkness side, and a best liberation of these emotion is a in-depth conversation with friend, but 100% not suicide to escape it.
* Theme of this week very interesting but maybe heavy for some people too, but this is great to let everyone face this topic squarely.

#2021-11-04
* This week we still dicussed about depression, but the core of discussion is how to help other people to solve it.
* Source of depression is extensive, can be social media, environment and so far.
* I am agree with Professors that social media is not recommended for people with depression.
* Actually I am not shocked that many of people in this class have depression in their life.

