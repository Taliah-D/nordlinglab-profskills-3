This diary file is written by Yong Jia Tan 陳勇嘉 E14085066 in the course Professional Skills for Engineering the Third Industrial Revolution.

# 2021-09-23 #

* The second lecture starts with an introduction on conflicts. It allows me to have a better understanding about how conflicts can be organized into different levels.
* Learned about GITHub and how it could be used as a platform to discuss and coordinate work.
* Learned about **Markdown**, which is commonly seen on Whatsapp for my case. Useful as an emphasis on certain terms.
* Acknowledged that there are a lot of objects in our surroundings that follow the rule of exponential growth leads to a world of abundance.

# 2021-09-30 #

* The third lecture placed an emphasis on statistics, which is a useful tool to allow people to know about trends and the world around them.
  - Main problem with statistics is that it could be easily manipulated by only obtaining desirable data or skewing the results of the data to suit certain contradictory conclusions.
  - Everyone should have the ability to differeciate between good data and bad data, not just believing every data that you see. Try to do your own research.
* Fake news is all over the place due to people who are lazy to fact-check the information that they received.
  - The same thing applies to statistics, be vary of any information presented and try to do your own research.
  
# 2021-10-07 #

* The fourth lecture gave a brief introduction on the Financial System, which is mostly based on credit after the abolishment of the Gold Standard by President Franklin D. Roosevelt.
* The current state of the global Financial System is heavily skewed towards the rich, benefiting them as the more money you have, the greater the power to amass more money.
* From the recent 2008 Financial Crisis, we can know the effects of an unregulated banking industry where credit has been used as a tool to make profit, but ended up hurting themselves instead due to mismanagement.
* I found it absurd about the fact that the professor brought up, which is millions of NTD could just appear from nothing by just taking a loan, which shows that this system has a terrible flaw, since it is not regulated by anything solid other than credit.

# 2021-10-14 #

* Populism is on the rise everywhere, especially in the Western world more recently, as people are getting more negative views on the so called "Threats" that would affect their wellbeing.
* Countries like Poland, Germany and Hungary has a sizeable following of populism and nationalistic societies.
* When you don't really know each other, you will be prone to misinformation being spread by intentional people, which is concerning since this would only reinforce their false beliefs onto certain groups.
* Marginalised groups are the groups that should get more care, but with misinformation being spread around they would only recieve hate.
* Malaysia, the country that I'm from, suffers from people not trusting each other, which has hindered it's development of being a power in South East Asia despite having the resources to become one.
* People should try and learn more about each other, so that we can live in a more compassionate world.

# 2021-10-21 #

* Stories are enjoyable for humans, everyone likes a good story! Yet still, there are plenty of stories that are simply made up, since you cannot really fact-check stories that are passed on by mouth.
* Usually stories passed through word of mouth will be altered, and depending in the severity of the alteration, it could cause conflicts.
* Stories are also used by people who has intentions to manipulate people's thinking, hence the rise in populism. Populism is basically the business of selling stories by Politicians.
* Emotions are hard to control, as you would not know when it became uncontrollable. Training your brain into controlling your emotions takes time and effort.
* Realised that in the grown-up world, we should have the ability to manage our emotions better, as more problem would arise as you grow older, that is why people tend to trust people who are more mature.
* Controlling emotions through breathing has always been the go-to advice for people who are getting nervous, yet still after knowing the science behind rhythmic breathing, it changed my mind of thinking breathing as a placebo to control your emotions.

# 2021-10-28 #

* Mental illnesses is something scary since unlike physical wounds, you cannot really determine the damage caused by mental illnesses.
* More and more people are suffering from mental illnesses in this current age due to unsatisfactory life or traumatic incidents. The industrialisation age did not help either since it has caused more competition among members of society to try and earn more to sustain their lifestyles.
* Solving mental illnesses is not an easy task since the human's understanding towards the human brain is still very shallow.
* The best medicine towards mental illnesses such as depression in my opinion is to have someone to talk to, as to let them feel part of society and not feeling lonely.
* The effects of depression are scary since in serious cases it may even lead to suicide, which is very saddening since most of the time the patients themselves don't really want to do that, they are just being manipulated by their emotions at the time.

# 2021-11-04 #

* Humans live in groups, and it is very important for humans to support each other, having each other's back when they needed it the most.
* The marketplace is highly competitive, sometimes people are being pressured to have to learn new knowledge or skills to increase their value. Yet still, sometimes even if you are a good worker, other factors also come into play.
* Factors not related to work such as the atmosphere of the workspace and the job scope of the workers also effect how well a worker can be suitable for the job.
* Yet still, it is too early to be overthink about the future, as self-improvement is much more important compared to be constantly worrying about the future.

# 2021-11-11 #

* Today a lot has been shared on the topic of climate change, and it's effects towards the world.
* Humans are constantly finding ways to combact these effects, including finding alternative ways to do something that is damaging towards the environment.
* Sadly the awareness on how the environment has changed for the past few years is still very low. Thus efforts to raise awareness must be continued to be done in order to gather the efforts of everyone.

# 2021-11-18 #

* The class for this week has been cancelled.




