* This diary file is written by YuChen_Hou E14081185

# 2021-09-24 (WEEK 2)#

* I still feel a little out of the loop in our second class.
* I'll try my best to keep up with my classmates.
* I finally contact with other member of our group.
* I used to hear of GIT, but I actually didn't use it. It's a good chance to learn about it.

# 2021-10-02 (WEEK 3) #

* A member of our group had dropped before our group meeting started, which means I lack a partner who can use the same language to collaborate. Fortunately, there is the help of Google Translate, I will try my best not to hold our group back.

* My partner is a nice person, we have successfully booked a fixed group work time after this class.

* The third class is about fake news and some details about citing statics we search on Internet.

* Unfortunately, the 3rd URL (https://newsvoice.com/) in the task after this class is broken, so that my partner and me can't answer it.
* A member of our group had dropped before our group meeting started, which means I 
  lack a partner who can use the same language to collaborate. Fortunately, there is the help of Google Translate, 
  I will try my best not to hold our group back.
* My partner is a nice person, we have successfully booked a fixed group work time after this class.
* The third class is about fake news and some details about citing statics we search on Internet.
* Unfortunately, the 3rd URL (https://newsvoice.com/) in the task after this class is broken, so that 
  my partner and I can't answer it.

# 2021-10-10 (WEEK 4)#

* The presentation of group 5 is interesting, especially the picture that Lev Kamenev is erased.
* Before today's class, I only had a vague understanding of the financial system. The inflation is something annoying but difficult problem.
* I had heard of QE because of the news about Fed's QE I accidentally browsed last year , but just had a simple concept that QE = print money.
* The line chart professor presented gave me the first intuitive experience of the depreciation of the new Taiwan dollar.

# 2021-10-17 (WEEK 5)#

* The first film reminds me of a movie I watched during a semester German class with my friends in high school: "Die Welle".
* Herd mentality is something to be wary of. Sometimes it becomes a hotbed of extremism.
* Belief is an interesting thing, usually influenced by the family of origin. It already exists in human culture.

# 2021-10-30 (WEEK 7)#

* People who has depression still desired interaction with other people.
* Treating them commonly, or it can become pressure to them.
* Listening is important. Listening might still has chance fail to avoid suicide, like the bridge police, but it would be better than inaction.
* Depression is a common things, we don't need to refuse it if we are suffuring depression. Receiving treatment calmly. Research indicates that 70% people get better after receiving treatment.

# 2021-11-13 (WEEK 9)#

* There was some chaos this week. Everyone was busy with midterm exams and finally completed our large group assignments before the deadline.
* We exchanged projects with the super group 2. After searching the information and editing the ppt together, we choose to draw lots, and I was selected to be responsible for presenting.
* When recording a youtube video, I chose screen recording. However, the 4:3 slides will have black borders, so I temporarily changed the ppt ratio to 16:9, but its results seem to be unsatisfactory.
* The professor mentioned that he was surprised why we did not quote the WHO standard. This is indeed our negligence. We chose the "European Heart Journal" and other professional research data, but when we introduced the basic standards, we forgot to follow the same standards.
* It was a very special experience that my presentation video was played in class, although it made me awkwardly reduced the volume of the computer and tried my best to ignore the sound of my own presentation.