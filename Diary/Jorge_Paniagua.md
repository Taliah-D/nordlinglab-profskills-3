This diary file is written by Jorge Paniagua E24097023 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* I am learning step by step how to use this new tool I dind't use before called Bitbucket.
* From the first group presentation I realized that almost anything in the world use batteries and this devices may provably become the main energy souces for everything in the world.
* For the future, it seems to be unlogical to invest in metro systems anymore, since using autonomous vehicles is getting cheaper and more convenient.
* I learned about the international standard for dates and time - ISO 8601.


# 2021-09-30 #

* Most of the time to have a better understanding of some statistic results you need to understand the relationship between the one who did it and the analized data.
* You need to ask yourself if you're related to the data.
* Take more in count public statistics over private statistics.
* Fake news may be more dangerous than real weapons.


# 2021-10-07 #

* Consider the possibility that history could be changed and manipulated through fake news.
* A certain part of Taiwan's growth is apparently thanks to loans.
* The world economy has a great dependence on the dollar.


# 2021-10-14 #

* If a company knows you much more than yourself, they can control and manipulate your own deepest emotions and desires without even realizing it.
* You should never underestimate human stupidity.  It is one of the most powerful forces shaping history.
* NTD purchasing power today is much lower than its purchasing power 30 years ago.


# 2021-10-21 #

* Exercise is good for improving memory.
* You cannot assume that a claim is true just because it cannot be proven that it is false yet and vice versa.
* I am impressed by the percentage of people who are worried, stressed and anxious about climate change.
* I think I already got used to using GIT repository, it already catched my attention.
* I may make some researchs in the future about more fuctions of Bitbucket in order to use the most of it. 

# 2021-10-28 #
* Depression is a very complex problem to take it easy, the best way to deal with it is by looking for professionals help.
* Some people may think that talking to people with depression is easy, but the truth is that any wrong word or sentence could trigger results that make the situation turn even worse.
* When someone(with possible depression) wants to share their emotions with you, just try to listen and understand them if you are not capable enough to continue the conversation.
* After this lecture I took some time researching about this topic and reading some articles and experiences of some people who went through situations related to it, I think it is necessary for people wants to help their friends but don't know how to help them.
* It is interesting to analyze the fact that a lot of chemical reactions can cause certain kinds of feelings in living beings. This topic led me to question myself about how we are affected by things we don't even knoe about. Also, assuming the fact that we are made up of atoms, which are supposed to be lifeless, but the set of a few atoms creates cells, molecules, organs, and human beings, which have life. How is it that a set of lifeless things can create life?


# 2021-11-04 #

* I learned about this new tool called MangaX
* I guess it might be hard to decide whether to stay or quit when you are in a good paid company with a bad work environment.
* "If you pit a good performer against a bad process, the process will win almost every time" I kinda liked this statement.


# 2021-11-11 #

* It is interesting to lean the definition of Legal Dictionary, although a bit annoying because it aroused my curiosity and I would like to have a good knowledge and handling of the words it contains since I feel that at some point in my life it might be useful for me as a basic tool, but since I don't have that much time available lately to memorize things, in the next few days I might probably just go throught my life normally ignoring its existence.
* I do not know how necessary some words are within the Legal dictionary, according to my knowledge some of the words that were used as an example have a very similar meaning to the ones we use in everyday language, what's more, nowadays there are dictionaries that classify words depending on the area in which they are to be used, so many words in the Legal dictionary are probably already defined in some other dictionary without necessarily being a Legal dictionary.
* At the end of the reading, I do not feel that one of the most useful features of a Legal dictionary is in redefining the meaning of certain words from everyday language to be used in the legal system.
* To some point, it seems to me that their meanings are still very close, and perhaps the same. 
* From my point of view, the only good purpose of having a Legal dictionary is more for convenience, since all the most frequently used words in the legal system are collected and compressed in the same place, which makes finding those words and accessing their meanings faster, much more efficient than looking for them in a common dictionary where there are much more combinations.
* Despite all of what I just wrote, I consider that the legal system is quite complex and it is very likely that it is more difficult for someone that has no any major related to the area to understand deeply the real importance of a Legal dictionary. Perhaps the difference is in certain expressions or terminologies to redefine a word, expressions that seem insignificant and imperceptible to someone like me, but that being in court has a great difference and greatly influences the result.
* It was a bit tiring to write this much, I should focus more on my second midterms but I'm tired after writing this diary xd. 


# 2021-11-18 #
Lecture.10 (No lecture due to professor need to give a speech in an activity)


# 2021-11-25 #
* I think that assuming the fact that each human being is alone on some planet separated from the rest would be much less problematic and more comfortable for each one, because it would not be necessary to apply unnecessary rules for a good coexistence.  Being alone somewhere would give one the opportunity to do what one wants without depriving another person of his personal rights.  But when we are living with other individuals, it is necessary to have rules taking into account the common good and that the sum of each individual benefit is the maximum.
* As a continuation of what has already been said before. Since we are coexisting with other individuals, I believe that these rules are more than necessary to have a reference line that no person should cross in order not to affect the rights of the another.
* When living with other individuals leads us to think about the need to reach a consensus to build certain rules, the only problem is that it is quite difficult to reach a consensus with individuals who in many cases have different ways of thinking and therefore their interests are different from the ones we might have.