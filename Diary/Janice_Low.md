This diary file is written by Janice Low B24095913 of the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* This is my first lecture. 
* I am inspired by the videos about fake news.
* I think we are living in a world full of fake news and information.
* The video "3 ways to spot a bad statistic" inpired me to think and identify before believing.
* It is essential to identify the source of the news.
* The source and ways are important when we do researches, prevent creating fake news.

# 2021-10-07 #

* It is fun to learn so much facts about money.
* The course today is a bit hard to understand for me.
* The teacher asked us "how to get millions of ntd legally" when he started the topic, I first thought the answer would be owning a bank, but eventually it is not.
* I started to think of the wealth gap problem today after the lesson. 

# 2021-10-17 #

* I am so into this topic.
* Speaking of fascism, I immediately think of China now a days. 
* It is horrifying to see people being kind of brain washed and have faith in their government blindly.
* As a Hong Konger, I am afraid that Hong Kong is gradually becoming a place filled with Chinese communist fascism.
* For me, the most unexceptable thing of fascism is is destroys people's free mind and culture.

# 2021-10-21 #

* It was shocking to know doing excersise can help growthing brain cells.
* When speaking of how to live healthily, I seldom connect it to the mental health part. 
* Matthias Müllenbeck's speech was inspiring.
* It is an interesting point of view of changing our existing health care systems from “sick care” to true “health care”.
* Although I don't think the technology now a days can offer people all over the world to moniter their health by AI, it is a facinating idea.
* Prevention is always better then cure, people's life expectancy can be longer if the technology and health care systems are mature enough.

# 2021-10-28 #

* I was moved by the video "The Bridge Between Sucide and Life".
* It really takes huge courage to live with depression, I was impressed by the mother mentioned in the video.
* However, though I agree with the idea of do not suffer from your depression in silence, I don't think everyone can be brave like the speaker.
* As one of the classmate being depressed without seeking for professional aids, I feels hard to open up my feelings to a stranger, even I known he/she is to help.
* Sometimes, the process of telling or reminding the wounded past is like getting through everything again, and it hurts.
* It is essential but difficult to seek for help and admit something is going wrong.

# 2021-11-04 #

* Big thanks to the ones shared their experiences in depression.
* Emily Esfahani Smith pointed out that chasing happiness can make people unhappy in her talk, which is so sarcastic but true.
* I have been reflecting on my life after the lesson.
* It is a fun process to think what makes me happy in my life and what is the purpose of it.

# 2021-11-11 #

* It was fun to work with groupmates from different countries, knowing the eco-friendly systems from other places.
* Duting the project, we found that the Taiwan government actually did a lot in compensating local Co2 emmisions.
* It was mid-term, so it is hard to find time to do more researches and dig deep into the topic.
* I was not feeling fine for 2 of my groupmates giving no ideas when we were having the onlinemeeting, they didn't even speak until we call their names and devided their parts. 