This diary file is written by Gabrielle Vania Tandjung H24075332 in the course Professional skills for engineering the third industrial revolution.  

# 2021-09-23 

* The Professor still discussed about conflicts on the second lecture, we were given more examples 
* There were some presentations on the second period, the Professor gave some inputs for improvements 
* The Professor also taught us how to use Github and how to make the diary properly 
* On the third period, the class discussed about the 4th Industrial Revolution that I didn't know about before 

# 2021-09-30 

* This week we talked about how to differentiate fake and real news 
* We also watched a video talking about how to spot bad statistics 
* As a Statistics student myself, that video was very interesting to watch 
* The professor talked again about diary and grouping problems since there are still some students who do not understand, me included 
* My group have not gotten the chance to present yet 

# 2021-10-07 

* This week we talked about economy and finance 
* The professor asked us to fill a questionnaire about economy and filling that made me realized that I have forgotten a lot of things about economy 
* Then we discussed more about the international and global economy situations 
* All of my previous group members dropped this class so I had to do the presentation for week 4 all by myself 
* Thankfully we are going to have new group arrangements starting next week

# 2021-10-14 
* This week we talked about humans and social lives and technologies 
* The will always be different social standings and powers inside the society 
* And we will have to be able to control it in order to prevent and avoid dictatorship 
* In terms of technological advancements, the technology are advancing in a fast pace during wars, which of course is extremely beneficial 
* However, technologies need to use wisely and we should always be careful when combining it witn human lives 

# 2021-10-21 
* This week we talked about how to live a healthier life 
* We also talked about stress levels and axiety and how they hinder our flow of works and ability to think
* There are ofcourse many ways to live healthily 
* However those claims have to have legit supporting evidences 
* Thus we have to keep our mindset stable in order to be able to live a healthier life 

# 2021-10-28 
* This week we talked about depression 
* We watched several TED videos regarding depression itself and people with depression
* It is said in the video that we should treat those with depression like usual and just talk like usual 
* We do not need to force ourselves to talk to them specifically about their conditions but just being there, knowing that it is okay and safe for them, is enough
* We should also understand that being diagnosed with depression is not a symbol of weakness and that we should not suffer alone 
* I do not personally know anyone that has depression but if one day I meet someone with depression, I hope I will be able to use the knowledge I gained from these videos to handle them accordingly

# 2021-11-04 
* This week we still talked about our mental health and issues 
* Sometimes we blame ourselves for our sickness but it might be affected by our environment 
* And that fact makes climate change a very serious issues 
* In order to live a healthier life, have a better mindset and mental awareness, we need to live in an environment that is as good or even better 

# 2021-11-11
* This week we did not do presentations with small groups but with the final big group instead 
* We watched all the presentation videos that each of the teams have made and submitted 
* All of the presentations are very nice and informative 

# 2021-11-18 
* There is no class for this week 
* My group decided to have a discussion about the next presentation on the supposedly class hour 

# 2021-11-25 
* Rules are needed since we are living with other people 
* Each of us is different, our needs and thoughts are special from each other 
* Thus we need to have the consensus
* The consensus need to accomodate all of our different needs 

