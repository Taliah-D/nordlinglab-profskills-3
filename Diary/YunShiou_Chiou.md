This diary file is written by YunShiou Chiou D84106035 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* This was the first time that I added in a course given by a foreign teacher. 
* I was confused about how to use git and bitbucket because it was my first time to use them.
* I was considering to drop the class because it was taught in English but what professor said in the class encouraged me to challenge myself.
* Being objective is not to treat everyone equally but to give everyone the equal chance to be listened.
* Stick to the information sources that you know or those are reliable.

# 2021-10-07 #

* "If you can't buy an apartment or a house, it is the fault of financial system, not yours."
* Bank actually borrow money from people(the deposit), and buy people's bond(the loan).
* When we take a loan, the bank print more money.
* If the amount of money is much more than the goods' value, it will cause financial crisis.
* We can print money by taking a loan!

# 2021-10-14 #

* People will be afraid of things they don't familiar with .
* We belong to a lot of places at the same time ,like Taiwan,Asia,and the earth .
* Embrace the differece between people and look at the similarties we share .
* See what are truly important to you and cherish them .
* There is no "liquid country" and"solid country" .

# 2021-10-21 #

* Sometimes, when  we change the way we solve problems, we can come up with new solution.
* Keep a habit of exercising every week, help us live longer and healthier.
* "Health care" is better than "Sick care".
* But change the relationship between doctor and patient must cost a lot.
* To take control of my performance, I have to take control of my physiology.

# 2021-10-28 #

* Depression is not contagious, when we talk with people live with depression, we just speak normally.
* Sometimes the biggest help we can give is to listen carefully. Being listened make us feel understood.
* Having emotions doesn't mean that we aer not strong enough, it means that we are human. 
* Because of the video I knew that a lot of people regreted since they let go the railing.

# 2021-11-04 #

* After listening to others' experience, I think it is an important thing to reach out our hand to those in need.
* If the working environment is not healthy and it makes you feel uncomfortable, try to speak out,and if communication is not working, just leave the environment as soon as possible.
* It's important to have good relationship with people in your working environment.
* Most of time, we made up the reason why we do something after people asked why.

# 2021-11-11 #

* Farming is a big burden, if all the crops grown were all directly consumed by people, world hunger could end.
* We can actually do thing to make the world a better place..
 
