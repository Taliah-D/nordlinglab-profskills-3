This diary file is written by Edwina Aurelia Saksana E14075142 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23

* First lecture and presentation day.
* I think exponential growth mostly applies to futuristic innovations.
* Exponential growth would stop when newer and better versions of the innovations appear.
* Professor teaches us that doing things according to templates is very important.

# 2021-09-30

* Professor uses Google Meet because it has subtitles feature and I think it's really thoughtful of Professor.
* More presentations, with some mistakes but we've gotten better from previous presentation.
* I learned how to verify news whether it's fake or real.
* It is important to verify news in order to prevent the spreading of rumours.

# 2021-10-7

* I like to learn more about finance, even if I'm majoring in Mechanical Engineering, because I think understanding how finance works is a very important aspect in life, that everyone should understand.
* Though Taiwanese finance is not so important to a foreigner like me, but I think knowing the basic knowledge of the world's finance is also important.
* A country's economy can inflate and deflate depending on debts and loans.
* During this pandemic, many people lose their jobs and have to depend on loans before finding new jobs, making so many countries' economy unstable.

# 2021-10-14

* We are currently living in fictional reality, some fictions are being made to reality, people live based on myths and superstitions.
* Fictional reality is an imagined version of our life that we hope or think is real.
* Even if fictions might not be true, there will always be lessons learned from why they were ever made in first place.
* Basically our lives and future lie in the hands of our government, or politicians in our country.
* From my home country, Indonesia, famous politicians have promised a lot of good things since decades ago, which I think would not be a problem if corruption in my country has ended.
* I think Taiwan's president also promises a very good future for Taiwanese, to be an independent country. Also, Taiwan is a rich country, so I think these promises are easier to achieve and to be done.

# 2021-10-21

* Professor talked about the physical experiment for reading heart rates and it's interesting.
* There are many ways for us to be healthy, not just sleeping well and eating well.
* By controlling our emotions, we can change our feelings in order to change our thinking, giving us more brilliant behaviour outcome.

# 2021-10-28

* Depression is not a joking matter, it is difficult to understand what everyone is going through.
* We have to pay attention to our mental healthas well as the mental health of the people around us, such as family and friends.
* Identifying depression in people is not as easy as identifying genders.
* We have to know what to do to help them with depression, even if it's not much, the little things and the good thoughts are what helps.

# 2021-11-4

* Professor said that social media is not recommended for people with depression because it acts like alcohol, the more likes we get the happier we are.
* I used to have a breakdown and I decided to take a break from social media for a few months, it actually helped me to stop being insecure.
* I think it is best to stay away from anything toxic in life during a breakdown, to prevent falling into depression, at least until you get yourself together again.

# 2021-11-11

* We watched all of the big teams' presentations about the course project.
* All of the presentations are quite long, but they're mostly still introduction, so they were all still kind of simple.
* One of the presentations talked about sleep being very important in our studies, as enough sleep helps us focus and concentrate better in class, which could help us improve our grades.
* Sleep is crucial in improving our memory.

# 2021-11-18

* No class today.