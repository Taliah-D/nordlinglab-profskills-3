This diary file is written by Pierre Chen E14069010 in the course Professional skills for engineering the third industrial revolution.

# 2019-09-12

* Course info was introduced by Professor Nordling.
* I got 44 in the pretest of this course.
* We are gonna do a 3 mins presentation on exponential growth.
* Our group chose semiconductors as our topic. 

# 2019-09-19

* I find the conflict view thing very useful.
* Jack gave us a tutorial of what Git is.
* Professor gave us some examples of disruptive innovation and exponential growth. (Wright's Law)
* The cost per mile is gonna drop by a lot in the very near future due to eletrification and autonomy of vehicles.
* Pick up one claim in the Ted talk given by Steven Pinker and find some evidence support or against it.(group HW) 

# 2019-09-26

* In the first two hours of the lecture we listened to the mini presentation for the arguments about the claims in Steven Pinker's Ted Talk from each group.  
* We watched two Ted Talk about truth and fake news. 
* The lecturer showed us different style of citation(e.g. APA citation) and why should we use citation...
* It is everyone's responsibility to verify the information they receive and even help spread out.

# 2019-10-03

* Classmates sharing their diary with us.
* Mini Presentation about fake news.
* Change our groups for Week 4-6. 
* Watch a couple of videos about economy.
* I strongly agree with those claims that money should go into things that can create value rather than things like real estate.
  Investment is investment only when you provide capital to a firm that can really produce something and generate profit for the value added, rather than pouring tons of capital into something with fixed value to make the price soar.

# 2019-10-17

* In general, nationalism is more stable and peaceful than non-nationalism.
* Some argue that nationalist becomes facism when going too far. Nationalist has sense of belonging to the values and identity of their country and feel proud, while facism monopoly its identity and exclude others often by colors or races.
* Facism and nationalism are fundamentally different.
* Before industrialization, land was the most valuable resource in human civilization. Then, machine was. Now, data is.
* AI revolution is causing data's concentration in hands of the few since it is more efficient.
* "Power corrupts and absolute power corrupts absolutely." — Lord Acton
* The way Chinese government is using AI technology like face recognition to monitor their people could be a huge threat to democracy.

# 2019-10-24

* Definition of knowledge by Merriam-Webster: the fact or condition of knowing something with familiarity gained through experience or association.
* Plato: "Well-justified true belief".
* Claim > testable hypothesis > evidence > knowledge or falsification
* Forming supergroup and practicing the path to knowledge.
* Most people in the class felt stressed or anxious this week.
* Demo cortical inhibition by measuring HRV and relates it to your stress level by rhythmical breathing.

# 2019-10-31

* Reminder of using bullet point to write diary and add the entry before 24:00 the same day as the lecture took place.
* Groups do mini presentations about rules of living healthy. 
* Check the format of the sildes that our task is to find a claim in the video and find an evidence against or support it.
* Videos about knowing yourself and meaningful life.
* I did not attend the class this week.

# 2019-11-7

* Two students share their diary.
* Mini presentation about depression.
* Ethos(trust or character of the speaker), Pathos(emotional connection to the audience), Logos(logical argument).
* Videos about happiness, fulfillment and forgiveness.
* Discuss all the claims in the videos and form testable hypotheses.
* Discuss what tangible means and come up with three tangible rules of how to live a happy/fulfilling life.

# 2019-11-14

* Students share their diaries.
* We listened to two mini problem presentations.
* Form two groups, one that have thought of changing external factor and the other that have thought of changing themselves.
* Students share what they regreted doing.
* Videos about legal system and remind us of our rights when the police ask you to provide personal info.
* Professor pointed out that as far as he knows there are two different kind of legal system in the world. 1. the UK system 2. the one France and Taiwan adopt.

# 2019-11-21

* If you find something boring, you shouldn't waste your time on it. Life is short, find other thing worth to spend your time on.
* Mini problem presentations on legal system and difference between civil law and common law.
* Professor did an experiment to show us how hard and how important it is to reach a consensus while get everyone stay connected.
* Watched three videos about social media's impact on millenials and the society. The last video remind us what an algorithm truly is.

# 2019-11-28

* We discussed the enforceability of a law and how difficult it is to come up with a proper law.
* We watched a couple of videos about monetary history and stupid mistakes that have been made again and again.
* Leaders don't read monetary history or inevitably there are always external factor forcing the leaders to make that tradeoff?
* US government export their inflation and dollars flowed into countries all over the world; that is, at the end of the day when all that money flows back into US(which is what Trump is trying to do now), FED will not be able to handle it by the four tools at that time.
* There is a cycle in market.
* Government should not intervene the market and should let the market heal itself.
* Look for news sources from other countries and see how they approach the same event differently.

# 2019-12-5

* We learned about planetary boundaries and that help me visualize how far it is to or has surpassed the sustainable line.
* Some of the boundaries are hard to quantified.

# 2019-12-12

* Presentations on planetary boundaries and social foundations.
* Debate and vote for an action that individual can take to help solve problems on planet boundaries and social foundations. 

# 2019-12-19

* Globally leading countries have been very keen manufacturing upgrade scheme.
* Revisiting the model for predicting technological progress (Moore's Law, Wright's Law, etc)
* Habits to a highly effective life.
* Watch a couple of videos about how to succeed and why to succeed.


# 2019-12-26 Thu.

1. Unsuccessful and productive.
2. I feel productive I read two articles about AI, watched a movie, studied linear algebra and listened to Noam Chomsky talking about Language, cognition and deep learning in a podcast. I feel unsuccessful because I could have done more if I got up earlier.
3. Get up early and avoid replying messages when studing.

# 2019-12-27 Fri.

1. Successful and unproductive
2. I feel successful because I finished a final project for another course; I didn't get up early and I went to bar with my friends til midnight after final project so I didn't read and listen to the podcast; thus, I feel unproductive.  
3. Get up early and finish up my assignments so that I can read and study.

# 2019-12-28 Sat.

1. Unsucessful and productive
2. I feel unsuccessful because I forgot our group discussion at 2pm in the library so I join in with voice call causing inconvenience to my teammates. I feel productive because I got up early and got a lot of things done in the morning. 
3. Get up early and exercise

# 2019-12-29 Sun.

1. Successful and productive
2. I feel successful and productive because I got up early to finish assignments for today so I got time to play basketball and hung out with friends for the rest of the day.
3. Get up early

# 2019-12-30 Mon.

1. Unsuccessful and unproductive
2. I took two days off and didn't go to taipei for my internship.I got up late and play video games with my friends for the rest of the day.  
3. Get up early and go to the library rather than stay at home

# 2019-12-31 Tue.

1. Unsuccessful and productive
2. I feel unsuccessful because I didn't feel like studying for final exams which I planned to do today but I feel productive because I learnt a lot of new things watching ocw and listening to podcasts. 
3. Get up early and get prepared for final exams 

# 2019-01-01 Wed.

1. Successful and productive
2. I feel successful and productive because I got up early to read, learnt a lot of new things watching ocw and got ready for the final exam for tomorrow. 
3. Get up early and exercise
# Five rules of success

* Do things that matter
* Take risks wisely and be brave
* Think critically
* Embrace failure
* Learn from mistakes

# 2020-01-02

* Professor shares the reason why he teaches this course.
* And that is to help us make impacts.
* In the last week, we have to prepare for the exam and make a video learning to make impacts and hopefully go viral.