*This diary file is written by Shu-Wen Guo (郭姝彣) E54091112


# 2021-09-30

* It was my first time in this class. As a result of not attending the the first two lectures, I have some difficulty in understanding what's going on; also,
 attending a all-english class isn;t a piece of cake for me, but I think I'll get better gradually.
* I think this class would be a lot better if it is physical. Hopefully we don't have to take online classes whole semester.
* This website is hard to use. It will be a lot simpler if we just use the website NCKU provides.
* From https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/, I learn more about news and ways to avoid taking in fake news.

# 2021-10-07

* I delivered a presentation today. Giving a presentation in a foreign language isn't easy for me; I spent a lot more time to prepare my text of speech than normal.
 Nevertheless, the result is good. I got the chance the show my work, and the presentation went out nicely.
* The topic of finance in this lecture is too hard for me, and the content seemed too much, since we're not able to watch those videos in class. However, this is
 still an nice lecture, I learn some interesting things; for example, how to print a million NTD legally.
 
# 2021-10-14

* I am glad that diary reading wasn't included in this week's lecture. Don't take me wrong. I don't hate hearing people sharing their diaries. However, I do think
  it is taking up too much time.
* The videos we are required to watch is interesting. I find it a little stupid how we humans live in fictions that we create, but it is also fun that we create
  myths for explaining the unknown, and rules for maintaining social orders.
  
# 2021-10-21

* These TEDTALKs are all informative, but among them I love the presentation that Wendy Suzuki gave. It was very fun, and I can totally felt her enthusiasm.
 I especially admire her including her own experience in the speech. It make it more relatable. Last but not least, the exercise she made the participants did at
  the end just gave the speech a perfect ending. I learn a lot from this video; not only the knowledge about how exercise can affect brain, but also tips for
  delivering a nice presentation.
  
# 2021-10-28

* My network is down in the middle of the class and I failed to catch up afterward. As a result, sadly, I didn't know what happened after we were divided into 
 supergroups.

# 2021-11-4

* The claim the professor proposed in the lecture that it's usually not one's fault if one has a bad time in a group is interestimg, and also kind of relieving.
  I had trouble interacting with my group members from time to time, and these experiences are not the happy ones. This claim may help soothe my anxiety.
  However, I also consider sometimes it is personal issue. After all, if one still can't fulfill one's work after thorough instruction, then there's is not much
  others can help.
  
# 2021-11-11

* Some of the presentations were very interesting, I especially liked the one about green building. How the prfessor questioned why NCKU is still building these 
 buildings which are not environmentally-friendly was also motivating.
 
# 2021/11/25

* I had fun discussing today. At first it was hard for us to think of any law, but then we decided to do something that had never been done. It was kind of over
 the top, and when the innovational idea came up, I was afraid that someone was going to mock on it. However, surprisingly, all group members agree on it. We ended up having
  a great time, and the class all cracked up on our presentation. It was such an interesting experience. I never had that kind of fun discussing or co-working.