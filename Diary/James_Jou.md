This diary file is written by james jou E34085036 for the course ~ Professional Skills for Engineering: The Third Industrial Revolution.

# 2021-09-25 #
* Learnt about GIT and how to use it
* Introduced to the professor and his background (both personal and educational)
* Got an insight on conflict escalation chart and its outcome
* How to sort conflict without causing more damage


# 2021-09-30 #
* Learnt on how to cite for our presentation (journals, website, etc)
* Detecting fake news
* How fake news are able to creat chaos in our society and drives people's opinion

# 2021-10-07 #
* Learnt on what gives money its value. 
* Learnt about how our financial system works
* Money is created by the bank when we are taking loans
* Bank has the power to create and print more money, however, extreme inflation can happen more money is created if there is a low demand for the currency
 
# 2021-10-27 #
* Learnt about facism 
* Which side is supported when the Taiwanese government rolls out the stimulus vouchers

# 2021-10-24 #
* learnt on how ECG can detect anxiety and nervousness 
* Learnt on how physical activities may improve our brain health
* Learnt on how our living conditions will impact our health both in a good way and a bad way
* Learnt how our surroundings may impact our anxiety

# 2021-10-28 #

* Learnt about how to communicate with those with depression
* Learnt on how suicidal thoughts may have impact on our lives
* Our close ones will me impacted when we suicide, hence if we want to do it, we really need to consider it
* Suicidal thoughts will come usually when we no longer see hope in our lives and yes I have experienced this myself

# 2021-11-4 #

* Found out that some of our classmates had ever dealt with severe depression and anxiety. 
* Some students including Taiwanese had point out that NCKU does not care about their student's mental health only until 3 people commited suicide in the course of 1 week
* A lot of students in this course were actually showing depressive symptoms 
* Learnt on how to deal with toxic work environment
* PAY < Well Being

# 2021-11-11 #

* It's a special "holiday" however all of my courses were still resumed as normal lmao
* Learnt that noise pollution can impose significant problem in our daily 
* "Green" building materials are not commonly used as no one market them
* Practices that harm the environment should be reduced in order to combat climate change.

# 2021-11-21 #

* No lecture this week! Used the time to watch the videos for the mini group homework


