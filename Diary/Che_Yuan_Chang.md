#### Professional skills for engineering the third industrial revolution - 第三次工業革命之工程專業技能 2021

---

##### Lecturer: Professor Torbjörn Nordling. Department of Mechanical Engineering #####
Email: [professor@nordlinglab.org](mailto:professor@nordlinglab.org) Office Phone: +886 (0)6 275 7575 Ext.62164

Office: Department of Mechanical Engineering National Cheng Kung University Office 91715 on 7th floor

##### Teacher Assistant: Ric Tu #####

Email: [ric.tu@nordlinglab.org](ric.tu@nordlinglab.org)

##### Tutor: Austin Su #####

Email: [austin.su@nordlinglab.org](austin.su@nordlinglab.org) Office Phone: +886 (0)6 275 7575 Ext.62159-73

Office: Mech. Eng. building room 91A02

> Course Web 

---

 -.Nordling Lab Web page: [https://www.nordlinglab.org/profskills/#](https://www.nordlinglab.org/profskills/#) 

 -.Nordling Lab Google Drive: [NordlingLab_Course_ProfSkills](https://drive.google.com/drive/folders/1qj8ECrgVRedn-.ctFGYLgXN190btKL6pJO)

 -.Nordling Lab Google Sheets (Group list): [NordlingLab_Course_ProfSkills_ParticipantsInfo](https://docs.google.com/spreadsheets/d/1FJ47smHB1JHGm_l5yGB0FVr8EQMDhTEKzvfjo4xibE0/edit#gid=0)

##### File title: __After Class Diary__ 

---

Recoder: Che-Yuan Chang. Student ID: F44085305. Department of Aeronautics and Astronautics

Contact: [Chang555cheyuan@gmail.com](mailto:Chang555cheyuan@gmail.com) (__Proper Work Only Please__)

If I have any mistake, missing something or you have any other point of view you want to share and dicuss with me please feel free to contact me.

The diary should be written and committed every week before Sunday 23:59, starting from the 2nd lecture, to a file with your name in the Diary folder. 

__Diary Content__

---

# 2021-09-16 #

#### Lecture.1: Introduction.(Unnecessary) ####

> Bullet Point 

  * __Knowing Basics information__ about the lectures, Professor's work experience, example research which do in the lab.
  * I __know too little__, I __should__ spend some time looking what is happen out there. 
  * If there is __question be brave to ask__ or else your are the one who will face more __obstacle__.
  * Ideas and Solution are not pop up with no reason, it comes with __observing__ problem, Surounding and life experience combine with your knowledge. 
  * Need to __pratice digest large amount of information is very short preriod of time__.
  * Need to Learn how to be more __open mind__, __creative__, learn some good __idea__ and __concept__ from __other people__.
  * Need to find a good __motivation__ and __inspiration__. 

Video watch: [HOW ABUNDANCE WILL CHANGE THE WORLD - Elon Musk 2017](https://www.youtube.com/watch?v=AgkM5g_Ob-w)

# 2021-09-23 #

#### Lecture.2 ####

 > Bullet Point 
 
  * Team work "__conflict condition__" problem solving method.  
  * Roughly know about GIThub, use Git through website for writing after class diary.
  * If there is difficulty to reach your teammate, you __Better__ to do all the jobs alone, no matter how difficult it is. 
  * __Standard of offical presentation slide oganization, copyright, reference and other precaution. __
  * Is good to __learn__ mistake now also try to __avoid__ it in the same time.
  * Try to __fully use__ your __tool to learn__ more useful information and follow up what is happen in the world.
  * Learn to collect real data for analysis the up going situation. 
  
Video watch: [Is the world getting better or worse? A look at the numbers | Steven Pinker](https://www.youtube.com/watch?v=yCm9Ng0bbEQ)

# 2021-09-30 #

#### Lecture.3 ####

##### Note #####

Statistic mean council of state, statesman or politicain, to better measure the population in the order to better serve it, so we need these goverment numbers but also havr to move beyond either blindly accepting or blindly rejecting these data.

__3 ways to spot a bad statistic:__ 

1. Can you see the uncertainty?        

2. Can I see my self in Data? (How you look the context)

3. How was data collected?  

Data is often better than you think, many people say data is bad, there is an uncertainty margin but we can see the difference are much bigger than the weakness of the data.

A big picture of statistic average is only for just reference for the big picture, if we look deeply into the detail will tell us inside of a big area have a lot of different in it, the sub area also can break down and so on, 
there is tremendous variation with in statistic data collection, which we very offten make that its equals everything.

This tell us, when we do some decision making we can not just us average to cover everything, we should look more detail find the different in each sub-area to make the correct or suitable policy within the area. 

Hans Rosling idea about the United statitic collection data ne is too idealist for present day however it may be possible in the future(now we have Google Trends),
in the present day this must be well organize, must give fair amount of payment for group of people who do these huge data collection until it can be automatically collected,
there is possible for giving partial free statistic data collection becasue these data is very valuable for soe people specially for big company for the future benefit, each data is only useful to specific person only, there is more space for discusion.

Christiane Amanpour wish the online platform can create a types of Algorithm that can filter out the fake news, it is quite difficult to filter out the fake news source, there is countless of fake news source on ther other hand is very easy to filter out
the information that goverment do what people to see, the most successful case is China and North Korea, the person who are in the power can easily control the new in the area they have control with, the fake news also happen pretty common in Taiwan, Thailand, 
Japan, korean, etc..., present day is extremely difficult to against the fake news, becasue we are not in control, some group of people make some new online platform to help people against this fake news even so it is very limited, with goverment and these people,
the best we online can do 90%, with the help of artificial automatic it maybe a very successful to control of fake news, or the opposite things will happen, then every country will be like china, we dont know, as a human we dont know what will happen who get the
power to be in control of something, just like the case of facebook which is suspect to sell privacy information to other company, to be optimistic, the futrue may be bright.

The people who wrote and spread the fake new they know, doing this is immoral or they just do the things that they believe in, there is too many ways to see through these kind of problem, maybe this is how they see the world, 
we wouldn't know, the best we can do is believe the thing we believe in, be open mind to accept other kind of point of view, and question with reason, keep the moral to ourself, there are no definite balck and white, there is the grey area too. 

The case of __SARS-CoV-2 (COVID-19)__ is unavoidable even in human history have encounter the similar case of diseases like black death, SARS-CoV Beta and MERS-CoV Beta
human cant avoid this kind of unseeable enemy, dont said Taiwan is the real example for showing its avoidable, the fact is Taiwan have the advantage for being a island country, if Taiwan is the country on the main land,
it will become a completely different story, i dont will not comment on the political part even there is some fact in it but is mostly the problem with peoples point of view and against to some specific group of people goverment or organization. 

__Adviser__: Professor Yang Bei-Chang Department of Medical 

> Bullet Point 

  * If we dont have any Statistic data, how can we Observe discimination, let alone fix it, not just about discimination, is about everything.
  * One way to make numbers more accurate is to have as many people as possible be able to question them.         
  * Goverment data collection is more accurate than the private company because their __Focus__ and __Power__ are __Different__.(__Not Always__)
  * Statistic data may not be 100% accurate still it is __very close to the accuratcy__.(if it was done by the proper creator)
  * __A good statistic collection can help us see how is the prsent world are changing, letting us see the fact that the world is not like how we think it is.__
  * Is __Dangerous__ to use the average data in a big area because such a lot difference within the subarea. 
  * How accurate is the statistic is depend on how you look and use the data you have.
  * Fake News mostly likely to be __Too Dramatic, Emotional and Click-bait__, the __True sometime is Borning__.
  * __Question with Reason__ to the information you see that is not in your profession.
  * The only way to filter or keep us from fake news in present, is depend on our own __Experience, Knowledge and Logic__.
  * Is very easy to miss lead people is by giving them what they want to see, that is Human Nature.
  * Is very difficult to let people do there own research to seek the true and avoid fake new, because most of the present day people are __too Lazy to read a long information by their own__.
  * __Don't forget history is wrote by the survivor, news can be change or erase by Money and Power__. 
  * One miss leading information can cause a domino effect miss leading to the society. 
  * Human have very limited ability to see through things, we only can see some of the part but not the whole part. 
  * We should always be __curious__ to seek the fact, looking in multiple source, find the most trustable source.
  * After of while of Practice is quite fun to use this webiste. 

Video watch: [3 ways to spot a bad statistic | Mona Chalabi](https://www.youtube.com/watch?v=Zwwanld4T1w)

Video watch: [The best stats you've ever seen - Hans Rosling](https://www.youtube.com/watch?v=usdJgEwMinM&t=911s) 

Video watch: [How to seek truth in the era of fake news | Christiane Amanpour](https://www.youtube.com/watch?v=iU1bhHeCkoU&t=602s)

Video watch:[Inside the fight against Russia's fake news empire | Olga Yurkova](https://www.ted.com/talks/olga_yurkova_inside_the_fight_against_russia_s_fake_news_empire#t-90580)

Video watch:[Debunking A Century of War Lies](https://www.youtube.com/watch?v=6y0RmLLjpHw)

# 2021-10-07 #

#### Lecture.4 ####

> Bullet Point 

  * In this lecture is quite difficult for me to understand, made me need more time to digest the knowledge with the video.
  * How the financial system works that I have study in this lecture is more useful than what i have learn in Thai elementary school.  
  * I just realize is very easy to made a financial and economic system collapse, when it have collapse is very hard to save it.
  * Surprisingling during Chinese new years in taiwan cause taiwanese bank Deposits and loans decrease exponentially in a moment just because most of the money went for chinese new year red envelope.
  * If the money in future become digitize globally will the financial system of creating money change? because there is no need to create physical money any more, good example is china which have fully digitize it money.
  * A large amount of __Transaction__ create a __Market__, a large amount of multiple types of __Market__ create the __Economic__.  
  * Economic just like a cycle it is normal to rise and also drop.
  * __Financial knowledge__ is very __Important__ for all of us because we are part of it and it is a part of our life. 
  * Is very difficult for me to help my teammate with this topic because i dont really understand this topic, even i want to try to help.
  
Video watch: [Money creation in the modern economy - Quarterly Bulletin](https://www.youtube.com/watch?v=CvRAqR2pAgw)

Video watch: [Richard Werner: Today’s Source of Money Creation](https://www.youtube.com/watch?v=IzE038REw2k)

Video watch:[How The Economic Machine Works by Ray Dalio](https://www.economicprinciples.org/)

# 2021-10-14 #

#### Lecture.5 ####

> Bullet Point 

  * __The Lecture about how Financial system work should be better improve, if necessary should spend little more time__ to dicuss of financial systemwork 
    because there still alot of people don't really understand and __understanding financial is important for our life__.  
  * This Lecture time is too less to learn the topic, which the time have been occupied by the previous lecture.
  * There is too much of misunderstanding definition between __Nationalism__ and __Fascism__ on most of the social media, so do most of the people misdefine them.  

-__Nationalism__: Love and devotion to one's country above all others.

-__Fascism__ : The idea that the group is more important than the individual.
  
  * From the human history is __normal for changing power of social political domination__, __it is very difficult to avoid the change notthing is forever__. 
  * It is true that __too much power and data was too centralized to small group of elite__, we can't really do anything becasue we are not specialize in that feild
    , we are not sure that is the change is good or bad, the social structure in __theory and practice always go differently__, the only thing we can do is always inspect the groverment,
    and give the job to the real specialist to deal with it but we can also learn how it is going.  
  * The things that __Yuval Noah Harari__ is worry about it actually is happening in our todays life, it is not excatly like what him said in present but it is going to day direction,
    there is already not alot of privacy when we start to have our first smart phone and connect to the internet.   
  * About the the merging Information technology with Biotechnology, is one of the research which most of the specialist and cooperation are focusing on, to study multiple studies like AI(Artificial Intelligence) or other bioloical,
    it is something that will be happen we can't stop, it is depend how people use this technology.
  * Is very difficult to separate the power equally in the social political domination, is better always to have opposite believe of socail party to control and warn the other, to make the power balance, preventing dictatorship.
  * The prevent of us to be control by the goverment or any organization is quite difficult now because now what we do and talk already been watch by the company of the social media we use today.
  * From the past to present day there is alot of film work, books and other that is warning us about the future, will that be true, we dont know. (ex. Terminator, The Matrix,Total Recall and Psycho-Pass)
  * Most of the people receive the warning information of the future technology is by watch or play entertaiment film and games, very few people receive from the expert.
  * The __Theory__ of citizen rise up againt the centrolize power goverment or dictator seem easy however during __Practice__ it is extremely difficult, some time don't even end up with happy ending.
    I think it is more a challenge if the goverment know what you think and do.
  * It is very extremely hard to aviod the any disaster which you can't avoid, even it already had happened in history, like black death compare the COVID-19 today, Soviet Union compare to China and North Korean today.
  * DO those crazy people know that what there doing is not good, for __Scientist__ of course they know they are not stupid, they even clear than most of us, but they what they can prevent it because there is no measurement that how far they can do
    and what will happen when they do, so the only way is to try and risk it, then study it for a long period of time.
  * In the time of war, the technology have advancing extremely fast but dont forget alot of thing we use in presentday was invented during the world war I and II, like antibiotic,...etc, it is not always the bad things if we change the way to use it, it will become very helpful. 
  * You can create something but you can not limit or control how other use it.
  * Every new Invention have no law or rule to control it, the only things we can do is to wait and let the new expert or we became the expert to decide it.
  * The person who in control that Yuval Noah Harari have mention mostly is not the profession in that feild so they can't not really understand how it work, the person who dicover it are the one who knows the best.
  * __Yes We should never underestimate the human stupidity__.
  * About any believe i wont have any comment, we all have the freedom to believe and join the arty we want, however we dont have the right to hurt or force other people.
  * We all need the place we belong, we are all kind of discriminate during our younger year, even so keep open to what you see and learn, be open and optimistic,
    by the time pass with the experience we have our mind will change or be the same we dont know, at least this is the way how we learn things, sometime there is more than meets the eye. 
  * Dont forget the knowledge and technology that we have today was built by alot of sacrifice there is no definite fairy tell happy ending. 
 
Video watch: [Why fascism is so tempting -- and how your data could power it | Yuval Noah Harari](https://www.ted.com/talks/yuval_noah_harari_why_fascism_is_so_tempting_and_how_your_data_could_power_it#t-3559)

Video watch: [My descent into America’s neo-Nazi movement -- and how I got out | Christian Picciolini](https://www.ted.com/talks/christian_picciolini_my_descent_into_america_s_neo_nazi_movement_and_how_i_got_out?language=en)

Video watch: [Yuval Noah Harari on Imagined Realities](https://www.youtube.com/watch?v=zen-m0rMp4I)

Video watch: [Yuval Noah Harari on the myths we need to survive](Video watch: [RSA Replay: A Brief History of Humankind](https://www.youtube.com/watch?v=2Vllgib842g))

Video watch: [RSA Replay: A Brief History of Humankind](https://www.youtube.com/watch?v=2Vllgib842g)


# 2021-10-21 #

#### Lecture.6 ####

> Bullet Point 

  * The __"Art of Speech"__ is to explain a topic for audient __"Easy To Understand"__ and use __"Short Period of Time For Explaining as Possible"__, not letting audient get bored.   
  * The Enviroment benefits product Original goal have been forgotten by the people, instead of the benefits to enviroment become waste to enviroment again.
  * The long culture belief of the people is very strong and extremely difficult to change, belief believe with conscious is fine but believe with no reason is the mot dangerous.  
  * There is multiple event that we can predict that will happen in the future that already happened in history but we cant predict exactly and with detil what will happen in the future, 
    it is also very hard to prevent the danger that will happen in the future, however we will try our best to prevent it. 
  * In this lecture I have find out one more disadvantage of the online class is difficult to join the physical experiment, there is different feeling when between we participate in the experiment
    and watching experiment video.
  * There is still too much of problem and conflict between goverment in each countries and there own citizen, where is the power go, what should goverment focusing be more focusing on and give some action for progress,
    which side is right which side is wrong, as we can see most or even all of the goverment leader are corrupted and only focus on what can benefit then, keeping them in power as long as possible.  
  * Every one have their own personal method to keep there mind and health balance, you can learn from other and customize in to your own.
  * You can't become better or brilliant by watching hours of video from professions, you need to have your own action for making yourself better.
  * You can take the professions speech as example not exact method becasue they are also human like us, thery also make mistake, one more thing is they don't know who you are and your condition, so dont alway believe what 
    is on internet, keep yourself conscious.
  * I personally think in present day we still very far from understanding how brain and consciousness work.
  * doctor are not the one keep us health, we are the one who keeps our own body and mental healthy, doctor are the one who tell us what is wronga and bring it to balance for us for short term, long term we are on our own.
  * doctor cant keep us always health, if doctor and pharma company make us health forever they wont have any job in the future, so they only keep us balance just like other industry in our society.
  * Health in each country have different history for causing the system they have today, is a very big challenge to change or improve it better for the people, specially in United States of America.
  * I hoping my self can have more time to do the exercise I like.
 
> Question 
 
  * Can large amount a data control and predict human decision exactly? 
  * Is human creativity and imagination start getting more limited?
  * Whos definition of knowledge is more correct and accurate bewteen, __Karl Popper "Conjectures and Refuations"__ and __Thomas Kuhn "Paradigm"__?
  * Can Human be more unpredictable by machine?    
  * Your Own biological data can tell you what is your feeling in the present time, however can this be trick?
  
Video watch: [TEDxPortsmouth - Dr. Alan Watkins - Being Brilliant Every Single Day (Part 1)](https://www.youtube.com/watch?v=q06YIWCR2Js)  

Video watch: [Wendy Suzuki: The brain-changing benefits of exercise | TED](https://www.ted.com/talks/wendy_suzuki_the_brain_changing_benefits_of_exercise#t-768811) 

Video watch: [What if we paid doctors to keep people healthy? | Matthias Müllenbeck](https://www.ted.com/talks/matthias_mullenbeck_what_if_we_paid_doctors_to_keep_people_healthy#t-108296) 


# 2021-10-28 #

#### Lecture.7 ####

> Bullet Point 

  * The Education system and Grading system for the student is never balance, never fair, we only can do what we can the best, is very difficult to against this types system which already been use for 15 centuries. 
  * Working in a larger group that combine with 3 to 4 small group of people is quite tough to discuss with others, since there were to many sound and very limited amount of time for discussion, if speak to much will create more problem,
    so sometime is better to keep quite, observe and learn how they solve the problem.
  * sometime for the people who dont really understand the situation keep quite, is also a kind of way to keep things simiple, let the person who understand most of the thing do there job, not always but in this situation of limited of time it fit.  
  * With the online course it is very difficult to find and dicuss with teammate with efficiency.
  * With increasing number of people who encounter depression and suicidal, as time past the age of person who encounter this problem have happen to younger and younger kid, this mean this kind problem is not individual, it mean our society is sick.
  * __"Medicine is a social science, and politics nothing but medicine at a larger scale"__ by Rudolf Virchow (13 October 1821 ~ 5 September 1902) 
  * Sad thing about __"How to connect with depressed friends"__ is very few people would share and agree with his idea, present day people are changing, becoming more like a machine rather than a emotional living thing, i agree with his idea but is less possible in this types of society.
  * I really respect how Mr. Bill Bernat choose to tallk about with depression, even with him self, speaking mostly very simple and easy but in practise is opposite of what we think.
  * Talking to a depressed person is not that easy as his said, it is a big challenge and responsibilities for that person, like the person who may said something wrong cause a irreparable lost, that person would carry it for the rest of his/her life.
  * TEDx is only a place for suggession, please watch with conscious.
  * Every single person have different life condition, so the advise speech from those sucessful person, may not suit with you since they are not you and you are not them, the best way to overcome this situation is to find your self, it may be a difficult task but we all need to go throgh this. 
  * We could never understand who have encounter depression, until we have experience our self that is true.
  * The person who End their own life or having a extremely tough time that they couldn't improve it, does not mean they are weak or not fit with the society, that is the experience that we all need to go through, I believe it it change to you, you couldn't make it either. 
  * There is no definite step or guide for helping a depressed friend even for the professions.
  
Video watch: [How to connect with depresed friend | Bill Bernat | TEDxSnoIsleLibraries](https://www.ted.com/talks/bill_bernat_how_to_connect_with_depressed_friends?language=en)

Video watch: [The bridge between suicide and life | Kevin Briggs](https://www.youtube.com/watch?v=7CIq4mtiamY)

Video watch: [Don't suffer from your depression in silence | Nikki Webber Allen](https://www.ted.com/talks/nikki_webber_allen_don_t_suffer_from_your_depression_in_silence?language=en)

# 2021-11-04 #

#### Lecture.8 ####

> Bullet Point 

  * __Climate change and global warming is a topic that we should take it very seriously__, it may not become a huge affect during our life time but how about the next generation, we better do some action that people before does not do, we are not like them leave the problem to the next generation. 
  * If we do some action it may be easier for the next generation to slove the problem, if not we just leave them with more problem, worst is they may leave this planet or human will go with extinction like the creatures before us. 
  * __I should better learn more about the Climate Change problem to consider that is true or not for not being mislead by any group of people.__
  * __I can't really judge or comment for which side is right because I am not special in this feild and completed knowledge about it.__
  * Our powerful leader and top richest people in the world who can affect whole global society are not stupid also not climatologists, they specialty is making business, maintain good relation with other countries leader, they can not be responsible for everything but responsible for most of the thing, so they need to thik very carefully before do any action, since they will be the one get blame the most.
  * __They have the resource does not mean they know how to use it even understand it__. 
  * Only blaming leader and goverment, forcing them to do something for the climate change is not best way because their is too much to consider, we can not just at one side of the thing we should look from the other side too.
  * The why how we apporoach sloving climate change __might be wrong__, __we are just human we may made mistake__, in present day __we may have not the best soluton for the climate change__ and have seek the __balance bewteem human society and mother nature__ .
  * About the climate change we should talk to the specialist from other field too, since this is not a small topic, we should consider alot of thing to make the best action that we can do in present day.
  * There may be alot of specialist is trying to find the best solution for climate change, that we didnt see.
  * __Don't just look at the thing you want to see, to see the truth is to look at the whole thing__.
  * Corruption is normal everywhere the difference is much or less.
  * Human will focus more on the things that have large market, create large amount of income and benefit to them self.
  * Goal of helping nature is good but sometime because of the market will create a new problem base of the goal of helping nature.
  * __we have the right to choose what we believe that is truth, there can be multiple truth someo time there is only one truth__. 
  * __Is very difficult to play game which the rule have been set by the society and there is more hopeless to agianst it__. 
  * No one teach you how to play the game of this society, even the sucessful person reach the place, tehy wont going to help the other because they dont want to share is benefit with other.
  * Base of the education is control by the people upon us, to create more dream worker they need for each different corporation.
  * We may not understand the a good advise from the elder now, we will understand the advise when we are in that situation. 
  * __You need to ask by your self when you need help, No one will ask you if you dont ask.__
  * __The real society is much more cruel than the society which mention in class__ becuase there is too much thing to consider. 
  * __Human mind and psycology is not simple as you think__. 
  * to gain turst by other in job is to do beyond your duty, so people will trust you that you are there whenever they need you.
  * __Don't always blame yourslef, sometime is the environmental problem__ that not suit with your style or it is not a good environment.   
  * __With the friendly environment will make your job much more easier and less pressure__.
  * If the working environment team make you have unconfident feeling, the best opption is to leave the place.
  * __If the we really want to learn something, the best is to choose a good and friendly environment to learn, there is extremely limited things or even notthing to learn in the toxic environment__.
  * There is no method that lead you to success or the definite right things to choose, it is only depend on your own believe and the value you choose in that moment, other people idea and way may not suit with you. 
  * If the environment is not good, you can start make it good by your self, if still not working is the best for you to leave this team.
  * Even the best team will become one of the worst team notthing is forever. 
  * There is both good team and bad team in the same big organization.
  * __Self knowledge may be is our self interpretation.__
  * __Know that you don't know yourself or at least not as well as you think you do From Daniel,N.(2013) magic person.__
  * May be we are not choosing what we want to do, we just explain our self.
  * May be since beginning, we all ready been control by other.
  * We should learn more and question more ourself to make th right choice we want to.
  * __Drink deep, or taste not the Pierian Spring; There shallow draughts intoxicate the brain, and drinking largely sobers us again.” ― Alexander Pope, An Essay On Criticism.__
  * __Kindness and forgiveness from all side, will be better for all of us.__
  * __Is the most difficult thing is to rise another human and teach him/her to become a "good person".__
  * The rightsuitable education and environment can make a life of new life better.
  * __Time wont go back the best we can do is make our self happy and better every day, last tell and teach other do not go wiht the same past as us.__ 
  * __Thinking too much or too less will make your life miserable.__
  * __There is not ultimate Truth, Purpose and Meaning in life, "Meaning and Purpose" in life is define by yourself not by other.__
  * __Even the worst person in the world still have the value that we can learn from this person.__ 
 
Video watch: [Do you really know why you do what you do? | Petter Johansson](https://www.ted.com/talks/petter_johansson_do_you_really_know_why_you_do_what_you_do?language=en)

Video watch: [What comes after tragedy? Forgiveness | Azim Khamisa and Ples Felix](https://www.ted.com/talks/azim_khamisa_and_ples_felix_what_comes_after_tragedy_forgiveness)

Video watch: [There's more to life than being happy | Emily Esfahani Smith](https://www.ted.com/talks/emily_esfahani_smith_there_s_more_to_life_than_being_happy)
  
# 2021-11-11 #

#### Lecture.9 ####

> Bullet Point 

  * There is better option of the environment, no matter it is cheaper or not,why we still didnt see a lot of this object in present time, there is many reason but one of the reason is the organization don't want spend a large amount of money to replace those old thing with new thing. 
  * Some time a organization have discover or create an invention become famouse, become famouse and well known by other is already enough for them, they will not try to improve thenself more. 
  * To organize a good team in university course is very difficult, dont even talk about communication cooperation and compromise.
  * I know some people feel pleasure from the noise of the motorcycle and go further with customize the pipe to make the lounder noise, it is agianst the law of most of the country, since thier still market for this kind of production so there still many place selling it.  
  * Next big group task look very challenging, making change.
  * This week is very hard for me to focus this lecture since the next day I am having a exam, and i am very sorry about that.
  
Video watch: [The reality of climate change | David Puttnam | TEDxDublin](https://www.youtube.com/watch?v=SBjtO-0tbKU)

Video watch: [Strawman - The Nature of the Cage (OFFICIAL)](https://www.youtube.com/watch?v=7sArXw6ajNg)

# 2021-11-18 #

#### Lecture.10 (No lecture due to profeesor need to give a speech in a activity) ####


# 2021-11-25 #

#### Lecture.11 ####

> Bullet Point 

  * I agree to with the hyrid lecture idea(Physical and online) however in pratice still face both human and techinqcal problem that we need to solve. 
  * As a present day human having a "__Physical lecture__" seeing and communicate other people wih them physically, made me have better connection with exchanging each of our point of view on the same problem with them, I more prefer physical class.  
  * By observing the class, there is a comman problem happen since kindergarten to university, that is talking during class, as a student we need to learn how to respect and pay attention to anyone who is speaking infornt the class. 
  * Anything which invovle with human there is always will happen problem to the human society (Morality, privacy, safty, etc...), this is unavoidable, since human society philosophy is very different compare to nature (after all we already different in the beginning ). 
  * As a normal user of internet, we havent fully use the maximum ability of the internet, the thing we see only is a extremly small portion of the activities on internet.  
  * In present day, there is a lot hacking, cyber attack, electronic warfare between country, goverment, personal hacker, crime organizations or even terrorism.
  * Jaron Lanier idea of internet sound very wonderful but I know he also now that society on interent is much more choas than want we think, is to difficult to reach what Jaron Lanier idea with internet because it will affect the interest of company and people with power.
  * I personally believe in the future, internet will not be much different compare to nowadays.
  * We need to understand what different field focus on and what is the goal that they want to reach, so we can understand each other and seek the balance on the same subject.  
  * No matter which kind of electronic devices, there is no privacy on interent, no matter you sign in or not, there still have code pin which is similar as your address also company will collect your personal data for getting more money from you, by selling more of there product or service to you that you will be internet in (the information is they secretly collect from you)      
  * Exchange or selling customer information between company is very common in nowadays, just someone us dont know about, no matter what reason.
  * When we sign the Treaty with any company we must read the infromation on treaty very clearly or else we can not protect our own right,(present day most of the customer will not look at it and agree with it, so the company can collect our information legally, using our information to maximize there benefit). 
  * Is near impossible to create a utopia of any subject, if our human system, society, idea, knowledge, culture is still like this  
  * The original mean of the education is good, but now the education system is just a system to create mindless worker for those rich people.
  * Any invention it all have it own use, it these invention dont deserve blame for causing problem for the human society, the person who are responsible is the user who use in the inhumane way.  
  * Our human society is get more sick in each day, there is not thing wrong with individual, just our mind is too slefish because if we care for other, the person who get hurt is our selves. 
  * __Life is not fair get use to it__ , in the futrue this kind of phenomenon will be nore worest then present day.
  * Improve your sef first then slowly change the environment, is very difficult, if no one start, no one will care, also manage your ability too.
  * We all have our own blind spot, we shouldnt only focus on the improve external object, we should also improving our internal (mental, idea) in the same time too， we are too focus on other thing and forget our slef, if we still dont improve our self, humanity society wont go to far.
  * I am very agree what Simon Sinek have said, even the simplest way for care others become a difficult task for people in this kind of the society.
  * Education that Education Bureau think that is very useful for student, is nearly useless for the student in the future.
  * The active and people who are full with enmotion become weirdo in present peoples mind.
  * Machine and animal become more enmotional, human become more like a heartless machine, who to blame? no one to blame.
  * Is very difficult to create a effective law which protect customers privacy and the benefit of the company, or we can sayto reach balance of the power.
  * I just notice if our own country have not follow the international treaty, we can sue the goverment to the court, however it depend on the country that the law is powerful than personal power or not.
  * If we human can not control self, no one can, human just like a create know they are killing themselves but still doing it.
  
Video watch: [How we need to remake the internet | Jaron Lanier](https://www.youtube.com/watch?v=qQ-PUXPVlos)

Video watch: [Simon Sinek on Millennials in the Workplace](https://www.youtube.com/watch?v=hER0Qp6QJNU)

Video watch: [The era of blind faith in big data must end | Cathy O'Neil](https://www.youtube.com/watch?v=_2u_eHHzRto)
 
# 2021-12-02 #

#### Lecture.11 ####

> Bullet Point 

  * 
  
  
# 2021-12-09 #

#### Lecture.11 ####

> Bullet Point 

  * 