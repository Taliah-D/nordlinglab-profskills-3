This diary file is written by Tyler Tang E84045044 in the course Professional skills for engineering the third industrial revolution.

# 2020-09-10 #

* I took a sick leave for the first lesson as I got a fever.

# 2020-09-17 #

* I found Steven Pinker's TED talk inspiring and I can totally relate the talk to a book that I have read called "Factfulness" written by Hans Rosling, Ola Rosling, Anna Rosling Rönnlund.
* I think the world is in generally getting better. And this claim can easily proved by data, because data do not lie provided the data are reliable.
* I think Markdown is a really useful tool that can make my texts really tidy and neat.
* I do not really get how GIT works and why tech companies adopt it. 
* I think it is hard to remember my thoughts, so the sooner I write the diary after/''''''''''''''''''''''' class, the better.

# 2020-09-24 #

* I think learning how to cite references is crucial especially when I am intended to further my studies in the future.
* I still don't know which one ("Source:" or "Credit to:") should be used to cite a reference when the graphies I showed were generated from a website.
* I got a bit bored and sleepy when watching long videos in classes.
* I found the ways provided in class and in the videos to detect fake news useful.
* I think the class this week was a bit in rush.

# 2020-10-01 #

* No class this week.

# 2020-10-08 #

* The topic this week: money is very interesting and closely related to our daily life. I did not know there are so many stories behind the notes I use every day.
* I learned how the economy roughly workes domestically and intenationally.
* The class did not offer total coverage of the money concepts due to the time limitation, but it was enough to provide space for us to fulfil our curiosity on this topic ourselves.
* I learned from this class that it is better to provide data as edvidences/references to form a sound solid arguement.

# 2020-10-15 #
* I learned that the main difference between nationalism and fascism is that in fascism the nation has the supremacy over anything.
* I think we should really be alert and concern about the gaining ability of centralizing and controlling data of the regimes and dictatorships, especially China nowadays.
* I learned that the skill of showing and telling figures and graphs is essential in workplaces, so it's better to take the chance to master it while in the shcool failures and mistakes are tolerated.
* I agree with Yuval that our world is constructed by fictional stories believed by most of us. 
* I think there are people that do not really believe/accept some fictional stories in our society, but they can hardly make a change of the systems already constructed.

# 2020-10-29 #
* I think it is not so easy and too general to draw a guide for how to spot a depressed person.
* Speaking of depression, I actually wondered in class how serious or common it is in Sweden and other Scandinavian countries.
* I agree with the video shown in class that listening, being there to support, acting normally are the best to do for most of us who do not really know how to handle depressed person. They works for your friends who have a sad moment too!
* I am still not so sure how to set up a testable hypothesis.

# 2020-11-12 #
* I  really never thought about how does the legal system work. 
* This course triggers me to figure out how does our current modern world work.

# 2020-11-19 #
* I gradually feel insecure in the fast-growing digital world.
* I think I am addicted to my smartphone. I need to find a way to mangage the time of usage.
* I think the algorithms for manipulating people, A.I. are as threatening as climate change and nuclear war in the future.

# 2020-11-26 #
* I think the presentation from Eric Cline is quite insightful from a historical and anthropological point of view that it warns us the same collapses could possibly happen nowadays.
* I think the topics covered in the letture seems to be overlapped with same topics introduced before.
* Though all of the topics are inter-connected.

# 2020-12-07 #
* The planetary boundaries seems a very practical concept that can be adopted to quantify the sustainable development, in particular the abstract SDGs.
* Human actions based on edvidences since the Industrial Revolution have become the main driver of global environmental change.
* Focus and work on the problem/ question itself. During the process of problem solving, alway refer to the problem/ question.

# 2020-12-14 #
* We concluded this week that climate change is most concerned in our class. One measure we figured out to help tackle the issue is a change to vegetarian diet. I am excited to hear about people might want to do it in class because I am a vegetarian myself.
* Third industrial revolution is new converge of communication, energy, transportation. For me I don’t even get to these topics well, not to mention the fourth industrial revolution people recently talking about.
* I think a constructive debate is based on sufficient researches and preparations, I could tell some of us were actually not so prepared.


# 2020-12-24 (Thu) #
* A. Successful and unproductive
* B. I felt successful because I went for a delicious meal and had a joyful Christmas Eve with my girlfriend. I felt unproductive because I did nothing much for my master's application and final exams.
* C. I will spend some time studying and researching.

# 2020-12-25 (Fri) #
* A. Successful and unproductive
* B. I felt successful because I celebrated Xmas with a brunch of people in An Ping. It was fun. I felt unproductive because I did nothing else today.
* C.  I will study, exercise tomorrow.

# 2020-12-26 (Sat) #
* A. Successful and unproductive
* B. I felt successful because I help celebrate my flatmate's birthday. I felt unproductive because I did nothing else today again.
* C. I will study, exercise tomorrow.

# 2020-12-27 (Sun) #
* A. Successful and unproductive
* B. I felt successful because I got invited to a friend's house to celebrate Xmas and I had a good evening with all the people. I felt unproductive because I still have not started to study and research for master's degree. I am stressed now
* C. I must start to study tomorrow, little by little is better than nothing.

# 2020-12-28 (Mon) #
* A. Unsuccessful and productive
* B. I felt unsuccessful because I feel like no one really understand me here, the ones who do are not in Taiwan. I felt productive because I finally started to study for my final exam and I went to excercise too.
* C. I will call my friends and family more often.

# 2020-12-29 (Tue) #
* A. Successful and unproductive
* B. I felt successful because I studied a bit but not postpone things. I felt unproductive because my studies today was not enough and it is cold out there.
* C. Keep warm and study hard.

# 2020-12-30 (Wed) #
* A. Unsuccessful and productive
* B. I felt unsuccessful and bad because I went to back at 4 AM. I felt productive because I did study for 4 hours today for an important subject.
* C. I will go to bed before 00:00 the next days, no matter what.