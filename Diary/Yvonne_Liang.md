This is a diary written by Yvonne Liang(梁瑄芳）E14081224

## 2021-09-30 ##

* I learnt how to use data in a proper way.
* Professor told about the importance of cite.

## 2021-10-07 ##

* We learn about how to check if it is a fake news.
* Someone asked in a team, we should listen to the leader or to be skeptical to everything, and the professor said we should trust ourselves.
* We had a quick view about financials.
* A poll result doesn't tell 100% of the truth.

## 2021-10-14 ##

* Today we learn about financial, the technical term for fainancial are still so difficult to understand.
* I learnt how to read a chart correctly, and also learnt how to determine the right chart for the aimed topic.
* I thought the inequality in Taiwan isn't a big issue, but after the presentation the other group made, I find it actually a problem now in Taiwan.

## 2021-10-21 ##

* I forgot to write down the diary for this week.
* This week we talk about fictions and learn about other country's stories.
* In one video presentation, I learnt that there's a system of sharing bottle in Taiwan now.
* Sharing economy is now more popular in Taiwan and is better for the environment, also it's a change in our lifestyle. 

## 2021-10-28 ##

* In todays lecture, we learn about health. Last week we talk about physical health and make presentation for this week then we learn about mental health for this week .
* We do a poll about eco-anxiety.
* I watched the ted-talk about how to connect with depressed people when we decided to. I get the answer just chat with them normally.
* In the other video, the speaker tell us when we have our loved one suicidal, what important to do is listen to understand.
* The Golden Gate Bridge in San Francisco with it gorgeous view turned out to be a bridge of suicide since its opening. 
* A suicidal person doesn't have hope in their Pandora's box. The hope can come from someone who hear them, no blame and just being there can be a turning point.

## 2021-11-04 ##

* Today we heard our classmats share how they overcome their depression, I think they are courageous inspirational.
* Many of us feel depressed often but most of the time we thought that we're just so called "Strawberry Generation". In fact we are depressed and need help immediately.
* We heard our classmate share about their experience on working. But to tell the truth, I fell asleep for a while and missed quite a few parts.
* We are reported the final result about the poll we did at week 7, which is about climate anxiety. Professor said the number of people that has climate anxiety is more than expected.

## 2021-11-11 ##
