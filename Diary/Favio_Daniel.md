# 2021-02-16#
* introduction to the course
* we made groups based on numbers which was an interesting way of grouping
* We watched a video of elon musk and I learned that the cost of things will go down by a large scale in the future

# 2021-02-23#
* we watched a ted talk
* i learned that overtime the cost of solar panel decreased and the cost of nuclear energy went up, so the solar energy is way cheaper now
* the gap between presentations are so long, which takes time of other people that have to do their presentation

# 2021-09-30#
* I was very confused about the group thing
* liked the first ted talk, the second was not very useful
* more people presented today
# 2021-10-07
* when using an image for presentation, you have to write the date you visited the website
* the first break was funny
* realised that I know little about financial system
* learned that money's value comes from our belief in it
* didn't get time to watch the videos so we're watching on our own.
* poor people are the one's making the money for society
# 2021-10-21
* Talk about exercise health benefits. 
* No matter what time you exercise, you'll get the benefits anyway
* learned how the emotions play an important roll in education and can make you learn slower or faster.
* got confused about the grading system.
* we did an interesting experiment, and realised many people are stressed.
* fasting is not for everybody, depends on the individual.
* poor people are the one's making the money for society.
# 2021-10-14
* New things will be to start recording the presentations, which is a good idea so will let more people have the opportunity to present.
* i think it will be on youtube
* we create imaginary things that lives among us
* nationalism creates hate between humans
# 2021-10-28
* realised that it is easy to talk to people with depression, but the thing is that we  often feel responsible for their depression, so instead of talking to them we just try not to.
* happy people are often not as happy as we might thing, on the contrary, they could be masking up their emotions to deal with reality.
* "people can be sad and okay at the same time". Really liked that phrase.
* All it takes is to listen to what somebody has to say, you don't really need to say anything, just listen. 
* Also took the time to watch another ted talk from the survivor of the golden gate, the one mentioned by Kevin Briggs and I think is worth watching to understand more about the whole story.